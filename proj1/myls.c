#define DIRECTORY 32
#define REGULAR 64
#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <linux/limits.h>

struct timestruct
{
    char week[4];
    char month[4];
    char day[3];
    char hr[3];
    char min[3];
    char sec[3];
    char year[5];
};

void MainModule(int arg_num, int argc, char ** argv);
void PrintPerm(mode_t * perm);
void PrintPermHelper(mode_t msk, mode_t permission, char c);

int main(int argc, char ** argv)
{
    int i;

    /* run on the current directory if no dir specified */
    if(argc == 1)
    {
        MainModule(-1, argc, argv);
    }
    /* loop through all the directories specified */
    else
    {
        for(i=1; i < argc; i++)
        {
            MainModule(i, argc, argv);
        }
    }

    return 1;
}

void MainModule(int arg_num, int argc, char ** argv)
{
    char filename[NAME_MAX];    /* saves filename */
    int total_blocks = 0;       /* counter for total blocks in dir */
    char pathname[PATH_MAX];    /* save full path name  */
    char dirpath[PATH_MAX];     /* another char array */
    char * delim = " :\n";      /* delimiter for strtok() */
    time_t curr_time;           /* used for getting current time */
    struct timestruct now;      /* to compare current time with file time */
    struct timestruct file;     /* file time */
    struct dirent * entry;      /* to parse directory */
    struct stat file_meta;      /* to get file meta data */
    struct group * grp;         /* to get name of the group id */
    struct passwd * pws;        /* to get name of the user id */
    DIR * dir;                  /* to parse directory */


    if(arg_num == -1)
    {
        dir = opendir(".");
        getcwd(dirpath, 200);
    }
    else
    {
        dir = opendir(argv[arg_num]);
        strcpy(dirpath, argv[arg_num]);
    }

    if(argc > 2)
    {
        printf("%s:\n", argv[arg_num]);
    }

    /* get current time and save the info to the time struct */
    curr_time = time(NULL);
    strcpy(now.week,strtok(ctime(&curr_time), delim));
    strcpy(now.month,strtok(NULL, delim));
    strcpy(now.day,strtok(NULL, delim));
    strcpy(now.hr,strtok(NULL, delim));
    strcpy(now.min,strtok(NULL, delim));
    strcpy(now.sec,strtok(NULL, delim));
    strcpy(now.year,strtok(NULL, delim));

    /* loop through the directory */
    while((entry = readdir(dir)) != NULL)
    {
        /* getting the full pathname of the file */
        strcpy(pathname, dirpath);
        strcat(pathname, "/");
        strcpy(filename, entry->d_name);
        strcat(pathname, entry->d_name);

        /* skip over hidden directories, ones that start with '.' */
        if(entry->d_name[0] == '.')
        {
            continue;
        }

        /* get meta-data for file, print error if an issue occurs */
        if(stat(pathname, &file_meta) < 0)
        {
            fprintf(stderr, "stat function failed\n");
            continue;
        }
        /* runs as long as meta-data for the file can be retrieved */
        else
        {
            /* count number of blocks */
            total_blocks += file_meta.st_blocks;

            /* getting file time and saving to time struct */
            strcpy(file.week, strtok(ctime(&file_meta.st_mtime), delim));
            strcpy(file.month,strtok(NULL, delim));
            strcpy(file.day,strtok(NULL, delim));
            strcpy(file.hr,strtok(NULL, delim));
            strcpy(file.min,strtok(NULL, delim));
            strcpy(file.sec,strtok(NULL, delim));
            strcpy(file.year,strtok(NULL, delim));

            /* printing permissions */
            PrintPerm(&file_meta.st_mode);

            /* printing number of links */
            printf(" %d", (int)file_meta.st_nlink);

            /* printing user */
            if((pws = getpwuid(file_meta.st_uid)) != NULL)
            {
                printf(" %s", pws->pw_name);
            }

            /* just print the user id if name cannot be retrieved */
            else
            {
                printf(" %d", file_meta.st_uid);
                /* printf(" ERROR"); */
            }

            /* printing group */
            if((grp = getgrgid(file_meta.st_gid)) != NULL)
            {
                printf(" %s", grp->gr_name);
            }

            /* just print the group id if name cannot be retrieved */
            else
            {
                printf(" %d", file_meta.st_gid);
                /* printf(" ERROR"); */
            }

            /* printing file size */
            printf(" %5d", (int)file_meta.st_size);

            /* printing time */
            printf(" %s %2s", file.month, file.day);

            /* print hour and minute if file year and current year match */
            if(strcmp(file.year, now.year) == 0)
            {
                printf(" %s:%s", file.hr, file.min);
            }

            /* print year if file year and current year don't match */
            else
            {
                printf(" %5s", file.year);
            }

            /* print filename */
            printf(" %s\n", filename);
        }
    }
    /* close directory */
    closedir(dir);
    /* print total number of blocks divided by 2, like ls -l does */
    printf("total: %d\n", total_blocks/2);

    if(argc > 2)
    {
        printf("\n");
    }
}

/* this function helps to print out *
 * the permissions of each file     */
void PrintPerm(mode_t * perm)
{
    mode_t temp = *perm;
    mode_t mask;

    /* get file type */
    temp = temp >> 9;
    if(temp == DIRECTORY)
    {
        printf("d");
    }
    else
    {
        printf("-");
    }

    temp = *perm;
    temp &= 511;

    /* get file permissions. loops   *
     * through the r,w,x values of   *
     * usr, grp, oth until all the   *
     * permissions have been checked */
    mask = 256;
    while(mask >= 1)
    {
        /* get r */
        PrintPermHelper(mask, temp, 'r');
        mask /= 2;
        /* get w */
        PrintPermHelper(mask, temp, 'w');
        mask /= 2;
        /* get x */
        PrintPermHelper(mask, temp, 'x');
        mask /= 2;
    }
}

/* helper function for printng permissions. runs a bitwise     *
 * and, which will check to see if the permission expected     *
 * to be on, is on. If it is on, it prints out the permission. *
 * otherwise it prints out '-'                                 */
void PrintPermHelper(mode_t msk, mode_t permission, char c)
{
    if((permission & msk) == msk)
    {
        printf("%c", c);
    }
    else
    {
        printf("-");
    }
}
