#ifndef MYSHELL_H
#define MYSHELL_H

#define MAX 82
#define PATH_MAX 1000
#define CTRL_A 1
#define CTRL_P 16
#define CTRL_D 4
#define DEL 127
#define EXIT_NUM 1
#define CD_NUM 2
#define PWD_NUM 3
#define SET_NUM 4
#define NOT_BUILTIN 0
#define _GNU_SOURCE
#define _XOPEN_SOURCE 500
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

struct command
{
    char cmd[MAX];                  /* command */
    char envar_path[MAX][MAX];      /* stores value(s) of enviornment variable */
    int use_envar;                  /* 1 if using and enviornment variable */
    int num_envar;                  /* num of enviornment variable values */
    char args[MAX][MAX];            /* arguments for command */
    int num_args;                   /* number of arguments for command */
};

struct io_redirect
{
    char file[MAX];             /* filename */
    int on;                     /* is i/o redirection being used */
};

struct job
{
    struct command cmds[MAX];   /* command list */
    int num_cmds;               /* number of commands */
    struct io_redirect input;   /* redirect input */
    struct io_redirect output;  /* redirect output */
    int is_bg;                  /* 1 if bg process, 0 if foreground */
};


/* redraws the '$ ' */
void RedrawBuff();

/* runs builtin commands */
void Builtin(int num, struct command * c);

/* returns a number that corresponds to a specific builtin */
int BuiltinNum(struct command * c);

/* executes the job */
void Execute(struct job * j);

/* helper function for Execute(), runs pipelined commands */
void Pipeline(struct job * j);

/* helper function for Pipeline() and SingleCommand(), calls execvp() */
void RunCmd(struct command * c);

/* parses command line and saves all infro into job struct */
void Parse(struct job * j);

/* helper function for Execute, runs single command */
void SingleCommand(struct job * j);

/* Signal handler for SIGCHLD. calls wait(NULL) to recover resources */
void SignalChild(int arg);

/* saves all the values of the enviornment variables to the command struct */
char * EnvarReplace(struct command * cmd, char * b, int * i, int * itr);

extern char buff[512];
extern char envar[MAX];
extern int fileinput;

#endif
