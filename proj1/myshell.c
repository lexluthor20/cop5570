/*************************************************************************/
/* code written with help from                                           */
/* http://www.cs.fsu.edu/~duan/classes/cop5570/examples/lect6/example9.c */
/*************************************************************************/

#include "myshell.h"

/* used for non-canoconial input */
char buff[512];
int curr_pos;
int fileinput = 0;

void SignalChild(int arg)
{
    wait(NULL);
}

int main()
{
    /* used for non-canoconial input */
    char ch;
    struct termios save_termios;
    /* used to restore input back to canonicalized before execution */
    struct termios old_termios;
    int read_from_file = 0;
    /* int curr_h; */

    int i = 0;
    struct job jb;
    jb.num_cmds = 0;

    /* get fields from the STDIN termios structure */
    if(tcgetattr(STDIN_FILENO, &save_termios) < 0)
    {
        /* perror("setattr"); */
        read_from_file = 1;
    }

    /* save a copy of the canonicalized termios structure */
    old_termios = save_termios;

    /* set non canonical mode and turn off echo */
    save_termios.c_lflag &= ~(ECHO | ICANON);
    for(i=0; i < MAX; i++)
    {
        buff[i] = '\0';
    }
    /* one byte at a time */
    save_termios.c_cc[VMIN] = 0;
    /* no timer */
    save_termios.c_cc[VTIME] = 0;
    /* set the new attributes that were just changed */
    if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &save_termios) < 0)
    {
        if(read_from_file == 0)
        {
            perror("setattr");
        }
    }

    curr_pos = 0;
    if(read_from_file != 1)
    {
        RedrawBuff();
    }

    while(1)
    {
        /* run once a char has been read */
        if(read(STDIN_FILENO, &ch, 1) > 0)
        {
            /* run if the char is a newline */
            if(ch == '\n')
            {
                /* write char to stdout */
                if(read_from_file != 1)
                {
                    putchar(ch);
                }
                /* clear buffer */
                fflush(0);
                /* set char after buffer's current position to null char */
                buff[curr_pos++] = '\0';

                if(curr_pos <= 81)
                {
                    /* restore canonical input for any system commands that *
                     * rely on canonical input, which is the default        */
                    if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &old_termios) < 0)
                    {
                        if(read_from_file == 0)
                        {
                            perror("setattr");
                        }
                    }
                    /* get job's cmd */
                    Parse(&jb);
                    /* execute job's cmd */
                    Execute(&jb);

                    /* set input back to non-canonical in order to get the *
                     * functionallity for the shell back                   */
                    if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &save_termios) < 0)
                    {
                        if(read_from_file == 0)
                        {
                            perror("setattr");
                        }
                    }
                }
                else
                {
                    fprintf(stderr, "ERROR: as per the project description,\n");
                    fprintf(stderr, "each input line from the user will have ");
                    fprintf(stderr, "no more than 80 characters\n");
                    fflush(0);
                    curr_pos = 0;
                    printf("$ ");
                    fflush(0);
                }

                /* flush buffer */
                for(i=0; i < MAX; i++)
                {
                    buff[i] = '\0';
                }
                /* print out a new shell input line */
                curr_pos = 0;
                fflush(0);
                if(read_from_file != 1)
                {
                    RedrawBuff();
                }
            }
            /* run if ctrl-A or delete is pressed */
            else if(ch == CTRL_A || ch == DEL)
            {
                /* backspace if curr_pos isn't 0 */
                if(curr_pos > 0)
                {
                    curr_pos--;
                    if(read_from_file != 1)
                    {
                        RedrawBuff();
                    }
                }
            }
            /* exit if CTRL_D is pressed */
            else if(ch == CTRL_D)
            {
                exit(EXIT_SUCCESS);
            }
            /* write to stdout */
            else
            {
                /* save all printable characters to buffer */
                if(isprint(ch))
                {
                    buff[curr_pos++] = ch;
                    if(read_from_file != 1)
                    {
                        putchar(ch);
                    }
                    fflush(0);
                }
            }
        }
    }

    return 1;
}

void RedrawBuff()
{
    int i;

    /* go back to start of line - NOT A NEW LINE, THE SAME LINE */
    printf("\r");
    /* print the shell input line */
    printf("$ ");
    fflush(0);
    /* overwrite the bad input with spaces */
    for(i = 0; i < 50; i++)
    {
        printf(" ");
    }
    /* go to start of line */
    printf("\r");
    /* null terminate the buffer holding the current command */
    buff[curr_pos] = '\0';
    /* print current command */
    printf("$ %s", buff);
    fflush(0);
}
