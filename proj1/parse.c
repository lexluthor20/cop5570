#include "myshell.h"

/* parse input */
void Parse(struct job * j)
{
    char c;
    char buffer[MAX];
    int i = 0;
    int k = 0;
    int buf_itr = 0;

    j->num_cmds = 0;

    do
    {
        c = buff[buf_itr++];

        j->cmds[j->num_cmds].num_args = 0;
        j->cmds[j->num_cmds].use_envar = 0;
        k = 0;
        i = 0;

        /* strip all spaces from start of input */
        while(c == ' ')
        {
            c = buff[buf_itr++];
        }

        /* get command */
        if(isalnum(c) || c == '/' || c == '$')
        {
            while(isalnum(c) || c == '/' || c == '$')
            {
                /* enviornment variable being used */
                if(c == '$')
                {
                    EnvarReplace(&(j->cmds[j->num_cmds]), buffer, &i, &buf_itr);
                    c = buff[buf_itr++];
                }
                /* regular command */
                else
                {
                    buffer[i] = c;
                    c = buff[buf_itr++];
                    i += 1;
                }
            }
            buffer[i] = '\0';
            strcpy(j->cmds[j->num_cmds].cmd, buffer);
            i = 0;

            /* get space before flags or arguments or pipe */
            while(c == ' ')
            {
                c = buff[buf_itr++];
            }

            /* get arguments and flags */
            while(c != '<' && c != '>' && c != '&' && c != '\0')
            {
                /* remove all spaces */
                if(c == ' ')
                {
                    c = buff[buf_itr++];
                }
                /* argument is inside quotes, save everything */
                else if(c == '"')
                {
                    c = buff[buf_itr++];

                    while(c != '"' && c != '\n')
                    {
                        buffer[i] = c;
                        c = buff[buf_itr++];
                        i+=1;
                    }
                    if(c == '"')
                    {
                        c = buff[buf_itr++];
                    }
                    buffer[i] = '\0';
                    strcpy(j->cmds[j->num_cmds].args[k], buffer);
                    j->cmds[j->num_cmds].num_args += 1;
                    k+=1;
                    i = 0;
                }
                /* command is piped - restart loop */
                else if(c == '|')
                {
                    i = 0;
                    break;
                }
                /* get argument */
                else
                {
                    while(c != ' ' && c != '\0' && c != '<' &&
                            c != '>' && c != '&' && c != '|')
                    {
                        buffer[i] = c;
                        c = buff[buf_itr++];
                        i+=1;
                    }
                    buffer[i] = '\0';
                    strcpy(j->cmds[j->num_cmds].args[k], buffer);
                    j->cmds[j->num_cmds].num_args += 1;
                    k += 1;
                    i = 0;
                    if(c == '<' || c == '>' || c == '&' || c == '|')
                    {
                        break;
                    }
                    c = buff[buf_itr++];
                }
            }
            j->num_cmds += 1;
        }

        /* store input redirection filename inside job */
        if(c == '<')
        {
            if(j->input.on == 1)
            {
                perror("input file already specified");
            }
            else
            {
                j->input.on = 1;

                c = buff[buf_itr++];

                /* consume all spaces before in */
                while(c == ' ')
                {
                    c = buff[buf_itr++];
                }

                /* save filename */
                while(isalnum(c) || c == '_' || c == '-' || c == '.' || c == '/')
                {
                    buffer[i] = c;
                    c = buff[buf_itr++];
                    i += 1;
                }
                buffer[i] = '\0';
                strcpy(j->input.file, buffer);
                i = 0;
            }
        }

        /* store output redirection filename inside job */
        if(c == '>')
        {
            if(j->output.on == 1)
            {
                perror("output file already specified");
            }
            else
            {
                j->output.on = 1;

                c = buff[buf_itr++];

                /* consume all spaces before out */
                while(c == ' ')
                {
                    c = buff[buf_itr++];
                }

                /* save filename */
                while(isalnum(c) || c == '_' || c == '-' || c == '.' || c == '/')
                {
                    buffer[i] = c;
                    c = buff[buf_itr++];
                    i += 1;
                }
                buffer[i] = '\0';
                strcpy(j->output.file, buffer);
                i = 0;
            }
        }
        if(c == '&')
        {
            /* background process */
            j->is_bg = 1;
        }


    } while(c != '\0');
}

char * EnvarReplace(struct command * cmd, char * b, int * i, int * itr)
{
    char * token;         /* used for strtok() */
    char temp[MAX];
    char temp2[PATH_MAX];
    char delim[] = ":\n"; /* used for strtok() to separate envar values */
    int j = 0;
    char c = buff[(*itr)++];
    cmd->use_envar = 1;
    cmd->num_envar = 0;

    /* save enviornment variable to temp */
    while((c != '/') && ((*i) < MAX))
    {
        temp[j] = c;
        c = buff[(*itr)++];
        j += 1;
    }
    temp[j] = '\0';

    /* check if getenv() is valid */
    if(getenv(temp) == NULL)
    {
        return NULL;
    }
    else
    {
        j = 0;
        /* copy string of enviornment variable values to temp2 */
        strcpy(temp2, getenv(temp));
        /* tokenize environment variable values */
        token = strtok(temp2, delim);
        while(token != NULL)
        {
            strcpy(cmd->envar_path[j], token);
            j += 1;
            token = strtok(NULL, delim);
        }
        cmd->num_envar = j+1;
    }

    return NULL;
}
