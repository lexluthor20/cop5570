#include "myshell.h"

/* global char array, stores the value of the enviornment variable */
char envar[MAX];

/* simple switch statement that runs the differing builtin commands */
void Builtin(int num, struct command * c)
{
    char * ptr; /* used for the "set" builtin */

    switch(num)
    {
        /* exit calls exit */
        case EXIT_NUM:
            exit(EXIT_SUCCESS);
            break;

        /* cd will change the directory to the first argument  *
         * if it exists. If none exist, changes directory to   *
         * the directory pointed to by enviornment variable    *
         * $HOME. If more than one argument exist, print error.*/
        case CD_NUM:
            if(c->num_args == 1)
            {
                chdir(c->args[0]);
            }
            else if(c->num_args == 0)
            {
                chdir(getenv("HOME"));
            }
            else
            {
                fprintf(stderr, "cd: Too many arguments\n");
            }
            break;

        /* pwd allocates space for the string returned by pwd *
         * to be saved to, prints the string, and then        *
         * recovers the dynamically allocated space           */
        case PWD_NUM:
            ptr = (char *) malloc(PATH_MAX * sizeof(char *));
            ptr = getcwd(ptr, (size_t) PATH_MAX);
            printf("%s\n", ptr);
            fflush(0);
            free(ptr);
            break;

        /* set copies the actual argument from the command line  *
         * into the global char array envar, then runs putenv()  *
         * which saves the char array as an enviornment variable */
        case SET_NUM:
            strcpy(envar, c->args[0]);
            putenv(envar);
            break;
    }
}

/* used to help determine whether the command is a builtin and   *
 * therefore if a child process should be made for the command.  *
 * Also, once the command is being run, identifies which builtin *
 * to actaully run                                               */
int BuiltinNum(struct command * c)
{
    /* run exit builtin */
    if(strcmp(c->cmd, "exit") == 0)
    {
        return EXIT_NUM;
    }
    /* run cd builtin */
    else if(strcmp(c->cmd, "cd") == 0)
    {
        return CD_NUM;
    }
    /* run pwd builtin */
    else if(strcmp(c->cmd, "pwd") == 0)
    {
        return PWD_NUM;
    }
    /* run set builtin */
    else if(strcmp(c->cmd, "set") == 0)
    {
        return SET_NUM;
    }
    /* command is not a builtin */
    else
    {
        return NOT_BUILTIN;
    }
}

/* executes the job by ensuring checking if the job is a   *
 * background process, seeing if the command is piped, and *
 * then calling the helper functions to acutally execute   */
void Execute(struct job * j)
{
    pid_t pid;

    /* these lines all deal with signals. The idea behind this *
     * is to set up a signal handler for background processes. *
     * when a child process dies, it fires off a SIGCHLD       *
     * signal. Normally, in a non background process, the      *
     * parent will be waiting for that signal before           *
     * continuing execution (i.e. wait(NULL)). However, with   *
     * background processes, the parent will continue          *
     * executing and not wait for the child to die. The signal *
     * handler therefore, will call wait(NULL) the second that *
     * the child dies to recover all the resources and prevent *
     * zombie processes. By making wait(NULL) a signal handler *
     * it only stops the parent from executing for a fraction  *
     * of a second to recover resources instead of waiting for *
     * the entire child process to run and die                 */
    struct sigaction sigchld_action;
    sigchld_action.sa_handler = SignalChild;
    sigaction(SIGCHLD, &sigchld_action, NULL);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);

    /* if the process is a background process, isolate it from  *
     * the parent before running i.e. make the actual command   *
     * a grandchild. Once the grandchild dies, the signal       *
     * handler recovers its resources, and then the child dies  *
     * from the exit(EXIT_SUCCESS) line of code                 */
    if(j->is_bg == 1)
    {
        /* make the child */
        pid = fork();

        if(pid == 0)
        {
            /* project stated that background process would not be *
             * used with pipelines, print out error if attempted   */
            if(j->num_cmds > 1)
            {
                printf("As per the project guidelines,\n");
                printf("pipe and background process will not appear together\n");
            }
            else
            {
                /* this command will make the grandchild, *
                 * which executes the command. When it    *
                 * dies, the SIGCHLD signal handler will  *
                 * recover its resources                  */
                SingleCommand(j);
            }

            /* this command kills the child. Child will *
             * likely die before grandchild since it    *
             * does not wait on grandchild.             */
            exit(EXIT_SUCCESS);
        }
    }
    /* background process is not being used, so run *
     * helper functions normally                    */
    else
    {
        if(j->num_cmds > 1)
        {
            Pipeline(j);
        }
        else
        {
            SingleCommand(j);
        }
    }

    /* reset values of the job struct to default */
    j->input.on = 0;
    j->output.on = 0;
    j->is_bg = 0;
}
/* pipe commands together */
/* for processes 0 through n-1, where n = num_cmds-1 */
void Pipeline(struct job * j)
{
    int i;
    int builtin_num;
    pid_t pid;
    int ** fd;          /* points to pipes */

    /* project stated that I/O redirection would not be used with *
     * pipelines, so print out an error if it is attempted.       */
    if(j->input.on == 1 || j->output.on == 1)
    {
        printf("As per the project guidelines,\n");
        printf("pipe and I/O redirection will not appear together\n");
        fflush(0);
    }

    /* create pipes dynamically. Since it is unknown how many *
     * commands will be piped together until runtime, wait    *
     * until runtime to dynamically allocate as many pipes as *
     * there are commands                                     */
    fd = (int **) malloc(j->num_cmds * sizeof(int **));
    for(i=0; i < j->num_cmds; i++)
    {
        fd[i] = (int *) malloc(2 * sizeof(int *));
    }

    /* iterate and pipe through all processes */
    for(i=0; i < j->num_cmds; i++)
    {
        /* get the builtin number for later use */
        builtin_num = BuiltinNum(&(j->cmds[i]));

        /* as long as the loop is not on the last *
         * command, create a pipe. Don't create a *
         * pipe for the last command because the  *
         * read-end of that pipe wouldn't be used */
        if(i != (j->num_cmds-1))
        {
            pipe(fd[i]);
        }

        /* create a child process */
        pid = fork();

        /* child process */
        if(pid == 0)
        {
            /* redirect stdout to write-end of pipe if not the last command. *
             * if this is the last command, the command should actually be   *
             * writing to STDOUT                                             */
            if(i < (j->num_cmds-1))
            {
                if(fd[i][1] != STDOUT_FILENO)
                {
                    dup2(fd[i][1], STDOUT_FILENO);
                }
            }

            /* redirect stdin to read-end of pipe if not the first command. *
             * if this is the first command, the command should actually be *
             * reading from STDIN                                           */
            if(i > 0)
            {
                if(fd[i-1][0] != STDIN_FILENO)
                {
                    dup2(fd[i-1][0], STDIN_FILENO);
                }
            }

            /* run command */
            RunCmd(&(j->cmds[i]));

            /* kill builtin commands. Since builtin commands *
             * are not using exec(), without calling exit(), *
             * the child would not die                       */
            if(builtin_num != NOT_BUILTIN)
            {
                exit(EXIT_SUCCESS);
            }
        }

        /* parent */
        else if(pid > 0)
        {
            /* wait for child to die */
            wait(NULL);

            /* without closing the write end of the pipe for the *
             * current process, the next process in the pipeine  *
             * would be waiting forever for the write end of its *
             * pipe to close just in case anything else was      *
             * written                                           */
            if(i != (j->num_cmds-1))
            {
                close(fd[i][1]);
            }
        }
    }

    /* after pipelining is finished, close the read-end of  *
     * all the pipes                                        */
    for(i=0; i < j->num_cmds; i++)
    {
        if(i != (j->num_cmds-1))
        {
            close(fd[i][0]);
        }
    }

    /* free up dynamically allocated *
     * memory used for the pipes     */
    free(fd);
}

/* runs the command by calling execvp */
void RunCmd(struct command * c)
{
    int builtin_num;
    char temp[PATH_MAX];    /* used when executing cmds with envars */
    char * argv[MAX];       /* used for execvp() */
    int i = 0;

    /* save builtin number for later use */
    builtin_num = BuiltinNum(c);

    /* it the command is a builtin, run Builtin() */
    if(builtin_num != NOT_BUILTIN)
    {
        Builtin(builtin_num, c);
    }

    /* runs if the command is not a builtin */
    else
    {
        /* copy the address of the command to argv[0] */
        argv[0] = c->cmd;

        /* copy the address of all of the command's *
         * arguments to argv                        */
        for(i=1; i-1 < c->num_args; i++)
        {
            argv[i] = c->args[i-1];
        }

        /* set the char * after the last argument to NULL, *
         * as required by the execvp command               */
        argv[i] = NULL;

        /* if enviornment variables are being used try them all */
        if(c->use_envar == 1)
        {
            /* for enviornment variables, iterate through   *
             * all of the possible enviorment variable      *
             * values, appending the command to the end of  *
             * them until one of the execvp() commands      *
             * with that string works. If none work, print  *
             * an error                                     */
            for(i=0; i < c->num_envar; i++)
            {
                strcpy(temp, c->envar_path[i]);
                strcat(temp, "/");
                strcat(temp, c->cmd);
                argv[0] = temp;
                if(execvp(argv[0], argv) == -1)
                {
                    continue;
                }
            }
            fprintf(stderr, "%s: command not found.\n", argv[0]);
            exit(EXIT_FAILURE);
        }
        /* enviornment variables not being used */
        else
        {
            if(execvp(argv[0], argv) == -1)
            {
                fprintf(stderr, "%s: command not found.\n", argv[0]);
                exit(EXIT_FAILURE);
            }
        }
    }
}
/* run a single command */
void SingleCommand(struct job * j)
{
    int fd_in;      /* used for file input */
    int fd_out;     /* used for file output */
    pid_t pid;
    int o_flag;     /* option flag for file i/o */
    mode_t mask;    /* mask for file i/o        */
    int builtin_num;

    /* save off the fd for stdin and stdout before replacing them */
    int old_fd_in = dup(STDIN_FILENO);
    int old_fd_out = dup(STDOUT_FILENO);

    /* get the builtin number for later use */
    builtin_num = BuiltinNum(&(j->cmds[0]));

    /* only fork if not a builtin */
    if(builtin_num == NOT_BUILTIN)
    {
        pid = fork();
    }
    /* runs for child or for built-in command */
    if((builtin_num != NOT_BUILTIN) || (pid == 0))
    {
        /* redirect input */
        if(j->input.on == 1)
        {
            /* open the job's input redirect filename as read only *
             * and save the fd to fd_in                            */
            fd_in = open(j->input.file, O_RDONLY);
            /* close stdin and redirect all input to the file */
            if(fd_in != STDIN_FILENO)
            {
                dup2(fd_in, STDIN_FILENO);
            }
        }

        /* redirect stdin to /dev/null for background processes if input is    *
         * not already redirected. This allows input to still be used in       *
         * background processes as long as it comes from a file and not STDIN. */
        else if(j->is_bg == 1)
        {
            fd_in = open("/dev/null", O_RDONLY);
            if(fd_in != STDIN_FILENO)
            {
                dup2(fd_in, STDIN_FILENO);
            }
        }

        /* redirect output */
        if(j->output.on == 1)
        {
            /* file will either truncate and write, or create a new file */
            o_flag = (O_WRONLY | O_CREAT | O_TRUNC);
            /* if a new file is created, set permissions to rw for usr */
            mask = (S_IRUSR | S_IWUSR);
            /* open job's output redirect filename */
            fd_out = open(j->output.file, o_flag, mask);
            /* close stdout and redirect all output to file */
            if(fd_out != STDOUT_FILENO)
            {
                dup2(fd_out, STDOUT_FILENO);
            }
        }

        /* run command. Since this is a single command, the *
         * first command of the job is hard coded since it  *
         * will always be there for single commands         */
        RunCmd(&(j->cmds[0]));

        /* builtin commands do not fork. For this reason,   *
         * after a builtin is finished running, it will     *
         * reach these statements. By running these dup2()  *
         * functions, any I/O redirection that was run will *
         * be reverted so that stdin and stdout will go     *
         * back to being used by the terminal instead of    *
         * files                                            */
        if(old_fd_in != STDIN_FILENO);
        {
            dup2(old_fd_in, STDIN_FILENO);
        }
        if(old_fd_out != STDOUT_FILENO)
        {
            dup2(old_fd_out, STDOUT_FILENO);
        }
    }
    /* parent */
    /* as long as the process is not a background command, *
     * wait for the child to die, recover its resources,   *
     * then continue                                       */
    else if((builtin_num == NOT_BUILTIN) && (pid > 0))
    {
        if(j->is_bg == 0)
        {
            wait(NULL);
        }
    }
    /* error */
    else
    {
        perror("unable to fork");
    }
}
