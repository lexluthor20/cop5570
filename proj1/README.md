**myshell**: a simplified version of Unix Shell program
=======================================================

functionallity 
=========================================================================

- supports input lines of length 80 or less.

- supports execution of all existing external commands in the system.

- supports ability to explicity call commands with enviornment variables,
    e.g. `$ENVAR/command`, `$ENVAR1/command1 | $ENVAR2/command2`.

- builtin commands: `exit`, `cd`, `pwd`, `set`
    for more on builtin commands, see below.

- supports variable number of pipes, 
    e.g. `command1 | command2 | ... | command n` however, I/O redirection
    and background processes are not supported with piped commands.

- for single commands, background processes are supported with the `&`,
    and I/O redirection is supported with `<` for input redirection and
    `>` for output redirection. Only one input and one output may be
    specified at one time. For more information on background processes,
    see below.

- `myls`: similar to `ls -l`, lists files under a directory. If there are
        zero arguments, command will list the current directory. For more
        information, see below.

built-in commands
==========================================================================

- `exit`: terminates the shell.

- `cd`: changes the current working directory. If there are zero arguments,
    command will automatically change the directory to the directory
    located at by `$HOME`. If there is more than one argument, then
    the error: `cd: Too many arguments` will appear.

- `pwd`: prints current working directory.

- `set`: sets an enviornment variable. Only supports one enviornment variable
    e.g. **MYPATH**, which is the search path for the implemented command `myls`.

background processes
=========================================================================

**myshell** does not support job control. Instead, when a command is
designated as a background process, **myshell** will first check to see
if there is input redirection from a file. If there is no input redirection,
it will redirect input from */dev/null*. If the command is a background
process, the parent does not wait on the child to die. Instead, the program
relyies on a `SIGCHLD` signal handler to recover the resources of the child
by calling `wait(NULL)` the second that the child dies. This prevents there
from being any zombie processes.

**note**: like the original bourne shell, when a program is run as a background
    process and the command prints to standard output, the `$ ` will be
    immediately followed by the output of the background process and the
    following input for the next command will not have the `$ ` at the
    start.

code
===============================================================================

the code for the shell is broken down into 4 files. **myshell.h**, **myshell.c**,
**parse.c**, and **execute.c**.

- **myshell.h**: the header file for the program. Contains all of the
    defines, includes, structures and function prototypes.

- **myshell.c**: this is where `main()` is stored. The functions that
    pertain to the actual typing into the shell is here, and after other
    functions are done running, they end up back here.

- **parse.c**: **myshell**, due to all the important characters e.g.
    `|`, `&`, `<`, `>`, `$ENVAR` coupled with the fact that each
    character does something different  and the fact that nothing 
    has to be space separated except for commands and arguments, made
    a custom parser seem the way to go. **parse.c** is a custom
    parser that sets up all of the internal data structures in my
    program so that once it is finished, the program knows:
    - whether piping is being done
    - whether the command is a background process
    - whether it is using input redirection, and if it is, what its filename is
    - whether it is using output redirection, and if it is, what its filename is
    - whether enviornment variables are used, and if they are, what their values are

- **execute.c**: this is where all of the commands are executed after
    the commandline is parsed. The program runs through either the function
    `Pipeline()` or `SingleCommand()` depending on if it is pipelined or
    not, and inside those functions the necessary changes are made when
    things like enviornment variables are being used, the command is a 
    background process, and when I/O is being redirected.

myls
============================================================================

**myls** is very similar to ls -l. The program prints out if the file is a
    directory or not, the permissions for each file, the number of links 
    that the file has, the name associated to the uid of the file, the
    name associated to the gid of the file, the file size, and the last
    modified time of the file. After the entire directory is finished, the
    total number of blocks used for all the files combined, divided by two
    is printed. This value is also shown in ls -l, however it is printed at
    the top not the bottom. If no arguments are specified, the program runs
    on the current directory. Otherwise the program runs on every argument
    e.g. `myls dir1 dir2 dir3 ... dirn`

**note on uid**: if a name cannot be associated with the uid, the uid will
    be printed out instead of a name.

**note on gid**: if a name cannot be associated with the gid, the gid will
    be printed out instead of a name.

**note on last modify time**: if the year of the file's last modified time
    does not match the current year, instead of printing the hour and
    minute of the file's modify time, it will print out the year instead.
