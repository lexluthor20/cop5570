#include "messenger.h"

struct thread_struct
{
    int fd;
    Server * s;
    sockaddr_in * addr;
};

string user_info_file;
int sockfd;
Server serv;
vector<int> sock_vector;

void SignalInterrupt(int arg)
{
    ofstream fout;
    unordered_map<string, struct UserInfo> table = serv.GetTable();

    //tell all users that server has quit unexpectedly
    auto usernames = serv.GetOnlineUsers();
    for(auto& x: usernames)
    {
        serv.ServerError(serv.GetUserFd(x));
    }


    //closing all open file descriptors
    close(sockfd);
    for(auto itr = sock_vector.begin(); itr != sock_vector.end(); itr++)
    {
        close(*itr);
    }

    //write table to file
    fout.open(user_info_file);
    if(fout.is_open())
    {
        for(auto i = table.begin(); i != table.end(); i++)
        {
            fout << i->second.username << "|";
            fout << i->second.password << "|";

            auto j = i->second.contact_list.begin();
            if(!(i->second.contact_list.empty()))
            {
                for(; j+1 != i->second.contact_list.end(); j++)
                {
                    fout << *j << ";";
                }
                fout << *j << "\n";
            }
            else
            {
                fout << "\n";
            }
        }

        fout.close();
    }

    exit(EXIT_SUCCESS);
}

short PortNumber(string filename);
void Testing(string filename);
int Register(int sockfd, Server * s, string input);
int Login(int sockfd, Server * s, string input);
vector<int>::iterator NewUser(int fd, Server * s, char * input);

int main(int argc, char * argv[])
{
    string config_file;
    int rec_sock;
    int error; //checks that functions worked
    socklen_t len;
    struct sockaddr_in addr, recaddr;
    fd_set allset, rset;
    int maxfd;
    char buf[SIZE];
    struct hostent * he;
    char hostname[1024];
    struct in_addr **addr_list;
    vector<string> usernames;
    vector<int>::iterator removefd;

    //signal handler for SIGINT
    struct sigaction sigint_action;
    sigint_action.sa_handler = SignalInterrupt;
    sigaction(SIGINT, &sigint_action, NULL);

    if(argc != 3)
    {
        cerr << "error" << endl;
        cerr << "usage: messenger_server ";
        cerr << "<user_info_file> <configuration_file>";
        cerr << endl;
        exit(EXIT_FAILURE);
    }

    user_info_file = argv[1];
    config_file = argv[2];

    //Filling up hash table
    serv.FillTable(user_info_file);

    //create a tcp socket and return the socket fd
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    //check that socket() worked
    if(sockfd == -1)
    {
        perror(": Can't get socket");
        exit(EXIT_FAILURE);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(PortNumber(config_file)); //port# from file
    addr.sin_addr.s_addr = INADDR_ANY; //let system select address

    //bind sockfd to system selected address and port
    error = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));

    //check that bind() worked
    if(error == -1)
    {
        perror(": Can't bind");
        exit(EXIT_FAILURE);
    }

    //used for printing ip and port numbers
    len = sizeof(addr);
    getsockname(sockfd, (struct sockaddr *)&addr, (socklen_t*)&len);

    //socket is now listening for an incoming connection
    error = listen(sockfd, 5);

    //check that listen() worked
    if(error == -1)
    {
        perror(": Can't listen");
        exit(EXIT_FAILURE);
    }

    hostname[1023] = '\0';
    gethostname(hostname, 1023);
    he = gethostbyname(hostname);
    addr_list = (struct in_addr **)he->h_addr_list;

    cout << "hostname: " << hostname << endl;
    cout << "ip address: " << inet_ntoa(*addr_list[0]) << endl;
    cout << "port: " << ntohs(addr.sin_port) << endl;
    cout << "number of users online: ";
    cout << serv.NumOnlineUsers() << endl;

/**************************************************************************
The following code was written with help from:
https://www.cs.fsu.edu/~duan/classes/cop5570/examples/lect14/example4_1.cpp
***************************************************************************/

    //clear the set
    FD_ZERO(&allset);
    //add listening socket to set
	FD_SET(sockfd, &allset);

	maxfd = sockfd;

	sock_vector.clear();

	while (1)
    {
		rset = allset;
		select(maxfd+1, &rset, NULL, NULL, NULL);

        //accept connections for new clients
		if (FD_ISSET(sockfd, &rset))
        {
			/* accept a new connection */
			if ((rec_sock = accept(sockfd, (struct sockaddr *)(&recaddr), &len)) < 0)
            {
				if (errno == EINTR)
                {
					continue;
                }
				else
                {
					perror(":accept error");
					exit(1);
				}
			}

			sock_vector.push_back(rec_sock);
            //add the new connection to the list of fds
			FD_SET(rec_sock, &allset);
			if(rec_sock > maxfd)
            {
                maxfd = rec_sock;
            }
		}

        //recieve data from clients
		auto itr = sock_vector.begin();
		while (itr != sock_vector.end())
        {
			int num, fd;
			fd = *itr;
			if (FD_ISSET(fd, &rset))
            {
                memset(buf, '\0', sizeof(buf));
				num = read(fd, buf, SIZE);
                //drop the client
				if (num == 0)
                {
					close(fd);
					FD_CLR(fd, &allset);
					itr = sock_vector.erase(itr);
					continue;
                }
                //process client data
                else
                {
                    removefd = NewUser(fd, &serv, buf);
                    if(removefd != sock_vector.end())
                    {
                        sock_vector.erase(removefd);
                        continue;
                    }

                    usernames = serv.GetOnlineUsers();
                    for(auto& x: usernames)
                    {
                        serv.SendLocationInfo(x, serv.GetUserFd(x));
                    }

                }
            }
            itr++;
        }

        maxfd = sockfd;
        if(!sock_vector.empty())
        {
            maxfd = max(maxfd, *max_element(sock_vector.begin(),
                        sock_vector.end()));
        }
    }
    return 0;
}

/*******************************************************************
end of borrowed code
*******************************************************************/

vector<int>::iterator NewUser(int fd, Server * s, char * input)
{
    string str = input;
    int value;
    int itr = 0;
    string decision;
    string username;
    string invited_name;
    string message;
    vector<string> user_list;

    itr = str.find_first_of("\n", 0);
    decision = str.substr(0, itr);

    str = str.substr(itr+1);

    itr = str.find_first_of("\n", 0);
    username = str.substr(0, itr);

    if(decision.compare("r") == 0)
    {
        //unable to register
        if(!Register(fd, s, str))
        {
            return sock_vector.end();
        }
    }
    else if(decision.compare("l") == 0)
    {
        value = Login(fd, s, str);
        if(value == 0)
        {
            return sock_vector.end();
        }

    }
    else if(decision.compare("logout") == 0)
    {
        //set user to offline
        s->SetOffline(username);
        //tell user's friends they are offline
        user_list = s->GetOnlineUsers();
        for(auto& x: user_list)
        {
            s->SendOfflineBroadcast(username, s->GetUserFd(x));
        }

        write(fd, "STOP_BROADCAST\n", strlen("STOP_BROADCAST\n"));

        //print out info
        cout << "-------------------------------" << endl;
        cout << "LOGGED OUT" << endl;
        cout << "username: " << username << endl;
        cout << "-------------------------------" << endl;
        cout << "number of users online: ";
        cout << serv.NumOnlineUsers();
        cout << endl;
    }
    else if(decision.compare("invite") == 0)
    {
        str = str.substr(itr+1);
        itr = str.find_first_of("\n");
        invited_name = str.substr(0, itr);
        str = str.substr(itr+1);
        itr = str.find_first_of("\n");
        message = str.substr(0, itr);

        //print out info
        cout << "-------------------------------" << endl;
        cout << "INVITE" << endl;
        cout << "from: " << username << endl;
        cout << "to: " << invited_name << endl;
        cout << "message: " << message << endl;
        cout << "-------------------------------" << endl;

        s->SendInvitation(username, invited_name, message);
    }
    else if(decision.compare("acceptinvite") == 0)
    {
        str = str.substr(itr+1);
        itr = str.find_first_of("\n");
        invited_name = str.substr(0, itr);
        str = str.substr(itr+1);
        itr = str.find_first_of("\n");
        message = str.substr(0, itr);

        cout << "-------------------------------" << endl;
        cout << "INVITE ACCEPT" << endl;
        cout << "inviter: " << invited_name << endl;
        cout << "accepter: " << username << endl;
        cout << "message: " << message << endl;
        cout << "-------------------------------" << endl;

        s->AcceptInvitation(username, invited_name, message);
    }
    else if(decision.compare("CLIENT_EXIT") == 0)
    {
        for(auto itr = sock_vector.begin(); itr != sock_vector.end(); itr++)
        {
            //find fd in sock_vector
            if((*itr) == fd)
            {
                //remove fd from vector
                //close(fd);
                //sock_vector.erase(itr);
                cout << "CLIENT HAS EXITED" << endl;
                return itr;
            }
        }
    }

    return sock_vector.end();
}

int Login(int sockfd, Server * s, string input)
{
    string username;
    string password;
    string ip_address;
    char c_ip[100];
    int itr = 0;
    short port;
    struct sockaddr_in location;
    vector<struct UserInfo> online;

    //get username
    itr = input.find_first_of("\n", 0);
    username = input.substr(0, itr);
    input = input.substr(itr+1);
    itr = input.find_first_of("\n", 0);

    //get password
    password = input.substr(0, itr);

    //bad login credentials
    if(!s->CheckLogin(username, password))
    {
        //send user error code 500
        write(sockfd, "500", 4);
        return 0;
    }

    //valid login credentials
    else
    {
        //save fd to hash table for later use
        s->AddUserFd(username, sockfd);

        //send error code 200
        write(sockfd, "200", 4);

        //get location info
        read(sockfd, c_ip, 100);
        ip_address = c_ip;
        read(sockfd, &port, sizeof(port));

        //save location info to sockaddr struct
        memset(&location, 0, sizeof(struct sockaddr_in));
        location.sin_family = AF_INET;
        location.sin_port = port;
        location.sin_addr.s_addr = inet_addr(ip_address.c_str());

        //update user info with location info
        s->AddLocationInfo(username, location);

        //set user to online
        s->SetOnline(username);

        //print out info
        cout << "-------------------------------" << endl;
        cout << "LOGGED IN" << endl;
        cout << "username = " << username << endl;
        cout << "ip address = " << inet_ntoa(s->GetLocationInfo(username).sin_addr);
        cout << endl;
        cout << "port = " << ntohs(s->GetLocationInfo(username).sin_port);
        cout << endl;
        cout << "-------------------------------" << endl;
        cout << "number of users online: ";
        cout << serv.NumOnlineUsers();
        cout << endl;

        return 1;
    }
}

int Register(int fd, Server * s, string input)
{
    int itr;
    string username;
    string password;

    //get username
    itr = input.find_first_of("\n");
    username = input.substr(0, itr);
    input = input.substr(itr+1);

    //get password
    itr = input.find_first_of("\n");
    password = input.substr(0, itr);
    input = input.substr(itr+1);

    //check if username is already taken
    if(s->UsernameAvailable(username) == 0)
    {
        //UNABLE TO REGISTER USER
        write(fd, "500", 4);
        return 0;
    }
    else
    {
        //USERNAME AVAILABLE
        write(fd, "200", 4);
    }

    s->AddUser(username, password);

    cout << "-------------------------------" << endl;
    cout << "REGISTERED" << endl;
    cout << "username: " << username << endl;
    cout << "password: " << password << endl;
    cout << "-------------------------------" << endl;

    return 1;
}

short PortNumber(string filename)
{
    ifstream fin;
    fin.open(filename, ios::in);
    string keyword;
    string value;

    if(!fin.is_open())
    {
        cerr << "Unable to open configuration file\n";
        exit(EXIT_FAILURE);
    }

    getline(fin, keyword, ':');
    getline(fin, value, '\n');

    if(keyword.compare("port") != 0)
    {
        cerr << "ERROR: Unknown keyword\n";
        exit(EXIT_FAILURE);
    }

    fin.close();

    return stoi(value);
}
