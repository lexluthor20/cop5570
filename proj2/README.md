Project 2: A Messenger Application
=======================================================

starting the program
=========================================================================

In order to build the program from source, run the command `make`
    inside the directory that the program is located at

In order to start the program, first begin by making a
    **user_info_file**. The file **user_info_file** contains the user
    account information. Each user account occupies one line. 
    Each line contains three pieces of user information: username,
    password, and contact list. They are separated by a vertical bar
    "|". Contact list may contain multiple friends (i.e., their
    usernames), which are separated by a semicolon ";". Passwords
    are in plain text. The following is an example line for user
    "user1", which has three friends in the contact list:

**user1|password1|user2;user5;user6**

After the **user_info_file** has been created, create the
    **server_configuration_file**. In order to do this, please 
    enter the word `port` followed by a colon `:` followed by a port
    number. This is the only thing that should be in the 
    **server_configuration_file**. The following is an example of a 
    **server_configuration_file**:

**port:4405**

After the **sever_configuration_file** has been created, you can run
    the server by issuing the command:

`./messenger_server user_info_file server_configuration_file`

where `user_info_file` and `server_configuration_file` are the
    file names of the `user_info_file` and 
    `server_configuration_file`.

Once the server is running you will see 4 things. A hostname,
     a ip address, a port number and a count of the number of users
     online.

To start up a client, begin by making a 
    **client_configuration_file**. to do this, make a new file and
    type the word `servhost` followed by a colon `:` followed by
    the ip address of the server. To get the ip address of the 
    server, look at the dotted number next to the **ip address: **
    printed out on the server screen. On the next line, type in
    the word `servport` followed by a colon `:` followed by the
    port number of the server. To get the port number, either
    look at the number next to the **port: ** printed out on
    the server screen, or look at the number saved in the
    **server_configuration_file**. These are the only two lines
    that should be in the **client_configuration_file**. The
    following is an example of a **client_configuration_file**:

**servhost:123.456.789.123**
**servport:1234**

where **123.456.789.123** is the server ip address, and **1234**
    is the server port number.

After the **user_configuration_file** has been created, you can run
    the client by issuing the command:

`./messenger_client client_configuration_file`

where `client_configuration_file` is the file name of the
    `client_configuration_file`.

you should now be able to run the messenger app. Feel free to
    make more client programs run by opening up a new terminal
    and running  `./messenger_client client_configuration_file`.

possible errors
=========================================================================

**server**

- If you start the server program and it says: `: Can't bind`
    then you should go into your **server_configuration_file**
    and try a different port number.

**client**

- If you start the client program and it says `: Can't connect`
    make sure that the server program is running, the ip address
    in the **client_configuration_file** matches the ip address
    of the running server program, and the port number in the
    **client_configuration_file** matches the port number of
    the running server program.

- If you login as a user and after the menu prints, it says
    `: bind: Address already in user`, it means that the port
    that the clients use to talk to each other is already being
    used on the system. This port number is hard codes as per the
    following project requirement:
    
    > After logging to the server successfully,  the client will then
    > create another socket using the socket() system. It will then
    > bind the socket to local port number "5100" and call listen()
    > to indicate that it is now ready to receive connect requests
    > from other clients for chat.

    In order to remedy this problem, you will need to update the
    port number in the code and rebuild the executables. To do this,
    open up the file **messenger.h** in any text editor i.e. **vim**,
    **nano**, **notepad**, **notepad++**, **emacs**, etc. and on line
    5, where the code says:

    `#define CLIENT_LISTEN_PORT 5100`

    update the number `5100` to a different number.

    After you have done this, save the file, and then run the
    command: `make clean` followed by `make`. If you run into
    the same issue, when you try logging in again, try a
    different number. If the project requirements were not so
    strict I would place the client listen port inside of the
    **client_configuration_file** to make changing this number
    easier.



functionallity 
=========================================================================

- supports input lines of length 1023 or less.

- supports the ability to register users.

- supports the ability to login users.

- supports updating the user_info_file before the server exits

- supports the ability for users to message each other,
    usage: `m friend_username whatever_message`

- supports the ability to invite users to be friends,
    usage: `i potential_friend_username [whatever_message]`

- supports the ability to accept invitations from users,
    usage: `ia inviter_username [whatever_message]`

- supports the ability to logout users,
    usage: `logout`

- supports I/O multiplexing using **select()** on the server side
    and **POSIX threads** on the client side

- supports the handling of `SIGINT` for both the server and 
    client when a user types `Ctr-C`

- properly closes all opened sockets

commands
==========================================================================

**before login**

`r`: Registers the user. When the user enters `r` they will be
        asked for a username and password. If the username
        supplied has not been registered on the server before,
        the screen will print out **REGISTERED** and the menu
        will be presented again. If the username has been
        registered before, the screen will print out
        **invalid login** and re-print the menu.

`l`: Logs in the user. When the user enters `l` they will be
        asked for a username and password. If the username
        supplied has been registered and the passwords match,
        a logged-in menu will appear, followed by all logged in
        users with their ip address and username. When the user
        gets a successful login, the client treats all incoming
        communications from the server as broadcasts, listening
        for when other users are online and offline, and if
        an invitation or invitation accept message is sent to them.

`exit`: Exits the program. When the user enters `exit` the
        socket file descriptor to the server will close and
        the program will end. When the client closes the
        server file descriptor, the server will remove close
        the fd on the server side and remove it from the list
        of file descriptors that it checks on.

**after login**

`m friend_username whatever_message`: When the user enters this
    command, a message will be sent to `friend_username` via the
    location information of the friend that the server supplies 
    when the user logs on, or a friend of the user logs on.

`i potential_friend_username [whatever_message]`: When the user
    enters this command, the message is sent from the person
    sending the invitation to the server, which looks to see
    if that user is online. If they are, the invitation is sent
    to that user and the invited user saves the invitation to
    a list of invitations, if an invitation from that user
    has not already been saved.

`ia inviter_username [whatever_message]`: When the user enters
    this command, the message is sent from the person who recieved
    the invitation to the server, which updates the friend list of
    both users, then looks to see if the inviter is online. If the
    inviter is online, the message is sent to the inviter. After
    this command runs, if both users are online, they will be able
    to direct-message each other with the 
    `m friend_username whatever_message` command.

`logout`: When the user enters this command, the client closes
    all file descriptors that are open (minus the fd between
    client and server), effectively ending all communicaation
    with other users. The client also switches from reading all
    incoming communication from the server as broadcasts to a 
    one-to-one communication with the server, to allow login and
    register functionallity to work.

**note on logout**: When the user types in `logout`, they are
    effecively loggout out of the system. All internal data
    structures with info saved about the user are removed
    and all connections are closed to that user. However, the
    actual file descriptor that allows communication to the
    server is maintained. This is to allow a new user to login
    to the server without having to end the program and re-connect
    to the server. However, nothing specific to the original user
    will be used for the new user logging in.

I/O multiplexing
=========================================================================

**server**: In order to handle communication with multiple clients,
    the server uses the **select()** system call. This is an approach
    that handles the messages between server and clients one at a
    time. In order to prevent delays to talk with the server from
    other clients, all information needed by the user is sent at once
    in one packet of text from the client to the server, delimited
    with newlines ('\n').

**client**: In order to allow each client to recieve messages from
    multiple users, recieve broadcasts from the server, and allow
    the user to send out chat (`m`), invitation(`i`), inviation
    accept(`ia`) and logout (`l`) messages simultaneously, the
    client uses **POSIX threads**. There are seperate threads
    that handle recieving incoming messages from each online
    friend, one thread that handles recieving and processing
    broadcasts from the server, and the main thread that handles
    `m`, `i`, `ia` and `logout` functions that the user types in.

signal process
=========================================================================

**server**: When a `SIGINT` signal is sent to the server, generated
    when a user types `Ctrl-C` into the server's terminal, The
    server begins by broadcasting a message to all online users
    stating that a server error has occurred. When clients recieve
    this broadcast, all users are automatically logged out and
    exited from the program. Next, the server closes all of its
    open file descriptors. Finally, the server takes current table
    of user information and writes the table back to the
    **user_info_file**.

**client**: When a `SIGINT` signal is sent to the client, the client
    is automatically logged out, which runs the `logout` command,
    and then the `exit` command. For more information on `logout`
    or `exit`, view the **commands** section of the README.

code
===============================================================================

The program makes user of two classes, **User** and **Server**.

**Server**: The main data structure for the Server class is a hash 
    table with the key being a string that is the username of a user,
    and the value being a UserInfo struct that contains the
    username, password, a list of all known contacts for the user,
    the location information of the user, an internal buffer for
    the user, the file descriptor that the server uses to
    communicate with the user, and an integer that is set to
    0 if the user is online and 1 if the user is offline. The
    class has a large amount of functions that allow the server
    to manipulate and use the data in higher level functionallity
    like telling a user all of their friends who are online.

**User**: The User class does not have a main data structure but
    instead a multitude of important data structures that are
    important for knowing the state of the user. This includes
    a hash table of online friends with the key being the friends
    username and the value being their location information, the 
    user's personal location information, a file descriptor to 
    communicate to the server, a list of all invitations recieved,
    username, password, and a list of all file descriptors that are
    open. The class has a large amount of functions allowing the
    user to user the data in high level functions like logging in,
    registering, and messaging online friends.
