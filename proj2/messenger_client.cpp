#include "messenger.h"

struct Arg
{
    User * user;
    socklen_t len;
    int fd;
};

User usr;

void SignalInterrupt(int arg)
{
    /*
    char buf[SIZE];
    memset(buf, '\0', sizeof(buf));
    strcat(buf, "logout\n");
    strcat(buf, usr.GetUsername().c_str());
    strcat(buf, "\n");

    //tell server user is logging out
    write(usr.sockfd, buf, strlen(buf));
    */
    //clear out data structures and close fds
    usr.Logout();
    usr.CloseServerFd();
    exit(EXIT_SUCCESS);
}

int Register(int fd);
void * ListenForConnections(void * arg);
void * NewConnection(void * arg);
void * ServerBroadcasts(void * arg);
void LoggedIn(User * user);
void Chat(int friendfd, struct sockaddr_in addr, char * friendname, string username, char * message, User * user);
void Menu();
struct sockaddr_in GetLocationInfo(int fd, string name);

int main(int argc, char * argv[])
{
    char temp_buf[SIZE];
    string config_file;

    pthread_t thread1;
    pthread_t thread2;

    //signal handler for SIGINT
    struct sigaction sigint_action;
    sigint_action.sa_handler = SignalInterrupt;
    sigaction(SIGINT, &sigint_action, NULL);

    if (argc < 2) {
        printf("Usage: messenger_client <configuration_file>\n");
        exit(0);
    }

    config_file = argv[1];

    //get server host and port
    usr.Initialize(config_file);

    while(1)
    {
        //print out login/register menu
        Menu();

        fgets(temp_buf, SIZE, stdin);

        //register routine
        if(strcmp(temp_buf, "r\n") == 0)
        {
            strcpy(usr.buf, temp_buf);

            if(usr.Register())
            {
                cout << "Registered" << endl;
            }
            else
            {
                cout << "Unable to Register -- name taken" << endl;
            }
        }
        //login routine
        else if(strcmp(temp_buf, "l\n") == 0)
        {
            strcpy(usr.buf, temp_buf);

            if(usr.Login())
            {
                //login passed
                //create a thread that accepts connections
                pthread_create(&thread1, NULL, ListenForConnections, (void *)&usr);
                //create a thread that gets server broadcasts
                pthread_create(&thread2, NULL, ServerBroadcasts, (void *)&usr);
                //print out logged-in menu and process decision
                LoggedIn(&usr);
            }
            else
            {
                //retry main menu if login fails
                cout << "invalid login" << endl;
            }
        }
        //exit routine
        else if(strcmp(temp_buf, "exit\n") == 0)
        {
            usr.CloseServerFd();
            exit(EXIT_SUCCESS);
        }
        else
        {
            cout << "Invalid entry" << endl;
            cout << temp_buf << endl;
        }
    }
}

void * ServerBroadcasts(void * arg)
{
    User * user = static_cast<User *>(arg);
    char buf[SIZE];
    int itr = 0;
    string str_usr;
    string str_ip;
    string str_port;
    string str_input;
    string packet_type;
    string str_msg;
    char c_ip[100];

    struct sockaddr_in temp;

    memset(&temp, 0, sizeof(temp));
    memset(buf, '\0', SIZE);
    memset(c_ip, '\0', 100);
    temp.sin_family = AF_INET;

    while(read(user->GetServerFd(), buf, SIZE) > 0)
    {
        str_input = buf;

        //get packet type from server broadcasts
        itr = str_input.find_first_of("\n", 0);
        packet_type = str_input.substr(0, itr);
        str_input = str_input.substr(itr+1);

        if(packet_type.compare("ONLINE") == 0)
        {
            while(str_input.size() > 0)
            {
                //get username of friend
                itr = str_input.find_first_of("\n", 0);
                str_usr = str_input.substr(0, itr);
                str_input = str_input.substr(itr+1);
                //get ip address of friend
                itr = str_input.find_first_of("\n", 0);
                str_ip = str_input.substr(0, itr);
                str_input = str_input.substr(itr+1);
                //get port number of friend
                itr = str_input.find_first_of("\n", 0);
                str_port = str_input.substr(0, itr);
                str_input = str_input.substr(itr+1);
                //save friend to online_friends
                strcpy(c_ip, str_ip.c_str());
                inet_aton(c_ip, &temp.sin_addr);
                temp.sin_port = stoi(str_port);
                user->AddOnlineFriend(str_usr, temp);
            }
        }
        else if(packet_type.compare("OFFLINE") == 0)
        {
            //get username of friend
            itr = str_input.find_first_of("\n", 0);
            str_usr = str_input.substr(0, itr);
            str_input = str_input.substr(itr+1);
            //remove friend from online_friends
            user->RemoveOnlineFriend(str_usr);
        }
        else if(packet_type.compare("INVITE") == 0)
        {
            //get username of inviter
            itr = str_input.find_first_of("\n", 0);
            str_usr = str_input.substr(0, itr);
            str_input = str_input.substr(itr+1);
            //get message from inviter
            itr = str_input.find_first_of("\n", 0);
            str_msg = str_input.substr(0, itr);
            str_input = str_input.substr(itr+1);
            user->AddInvitation(str_usr, str_msg);
        }
        else if(packet_type.compare("ACCEPT_INVITE") == 0)
        {
            //get username of inviter
            itr = str_input.find_first_of("\n", 0);
            str_usr = str_input.substr(0, itr);
            str_input = str_input.substr(itr+1);
            //get message from inviter
            itr = str_input.find_first_of("\n", 0);
            str_msg = str_input.substr(0, itr);
            str_input = str_input.substr(itr+1);
            user->AcceptInvitation(str_usr, str_msg);
        }
        else if(packet_type.compare("STOP_BROADCAST") == 0)
        {
            return NULL;
        }
        else if(packet_type.compare("SERVER_ERROR") == 0)
        {
            cout << "ERROR: SERVER HAS QUIT UNEXPECTEDLY" << endl;
            cout << "CLOSING ALL CONNECTIONS AND FORCING LOGOUT";
            cout << endl << "GOODBYE" << endl;
            user->Logout();
            close(user->GetServerFd());
            exit(EXIT_SUCCESS);
        }

        memset(buf, '\0', SIZE);
    }

    //pthreads require a return NULL
    return NULL;
}
void * ListenForConnections(void * arg)
{
    User * user = static_cast<User *>(arg);

    int error;
    socklen_t len;
    int sockfd;
    struct sockaddr_in addr;
    int iSetOption = 1;
    char endchat[SIZE];

    pthread_t thread;

    memset(endchat, '\0', sizeof(endchat));
    strcat(endchat, "\n\n\nCLOSE_CONNECTION\n\n\n");

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    user->AddToFdList(sockfd);
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (char*)&iSetOption, sizeof(iSetOption));

    if(sockfd == -1)
    {
        perror(": Can't get socket");
        user->Logout();
        user->CloseServerFd();
        exit(EXIT_FAILURE);
    }

    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(CLIENT_LISTEN_PORT);

    error = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));

    if(error == -1)
    {
            perror(": bind");
            user->Logout();
            user->CloseServerFd();
            user->CloseServerFd();
            exit(EXIT_FAILURE);
    }
    user->already_listening = 1;

    len = sizeof(addr);
    error = getsockname(sockfd, (struct sockaddr *)&addr, &len);

    if(error == -1)
    {
        perror(": can't get name");
        user->Logout();
        user->CloseServerFd();
        exit(EXIT_FAILURE);
    }

    error = listen(sockfd, 5);

    if(error == -1)
    {
        perror(": Can't listen");
        user->Logout();
        user->CloseServerFd();
        exit(EXIT_FAILURE);
    }

    struct Arg arg2;
    arg2.user = user;
    arg2.len = len;
    arg2.fd = sockfd;

    while(1)
    {
        if(user->TerminateThread())
        {
            close(sockfd);
            break;
        }
        //creates a new thread for each connection
        pthread_create(&thread, NULL, NewConnection, &arg2);
    }

    for(auto itr = user->GetOnlineFriends().begin(); itr != user->GetOnlineFriends().end(); itr++)
    {
        write(itr->second.sockfd, endchat, strlen(endchat));
    }

    close(sockfd);
    return NULL;
}
void * NewConnection(void * arg)
{
    struct Arg * arguments = static_cast<struct Arg *>(arg);
    User * user = static_cast<User *>(arguments->user);
    socklen_t len = static_cast<socklen_t>(arguments->len);
    int sockfd = static_cast<int>(arguments->fd);
    char buf[SIZE];
    string str;
    string name;
    string message;
    int itr;

    int rec_sock;
    struct sockaddr_in recaddr;
    struct sockaddr_in dummy;

    //accept connection
    rec_sock = accept(sockfd, (struct sockaddr *)(&recaddr), &len);
    user->AddToFdList(rec_sock);

    while(read(rec_sock, buf, SIZE) > 0)
    {
        if(strcmp(buf, "\n\n\nCLOSE_CONNECTION\n\n\n") == 0)
        {
            break;
        }
        if(user->TerminateThread())
        {
            return NULL;
        }
        str = buf;
        itr = str.find_first_of("\n");
        name = str.substr(0, itr);
        message = str.substr(itr+1);

        cout << name << " >> " << message << endl;
        memset(buf, '\0', sizeof(buf));
    }

    //if code gets here, connection has closed on other end
    close(rec_sock);

    //tell other side to close other side
    memset(buf, '\0', sizeof(buf));
    memset(&dummy, '\0', sizeof(dummy));
    strcat(buf,"\n\n\nCLOSE_CONNECTION\n\n\n");
    write(user->GetOnlineFriend(name,&dummy), buf, strlen(buf));

    return NULL;
}

struct sockaddr_in GetLocationInfo(int fd, string name)
{
    struct sockaddr_in temp;
    short  port;
    char str[100];
    char ip[100];

    strcpy(str, name.c_str());
    write(fd, str, 100);

    //get ip
    read(fd, &ip, 100);
    fflush(0);

    //get port
    read(fd, &port, sizeof(port));

    //save to sockaddr_in
    temp.sin_family = AF_INET;
    inet_aton(ip, &temp.sin_addr);
    temp.sin_port = port;

    return temp;
}

void LoggedInMenu()
{
    cout << "LOGGED IN" << endl;
    cout << "chat: m <friend_username> <message>" << endl;
    cout << "invitation: i <potential_friend_username> ";
    cout << "[whatever_message]" << endl;
    cout << "accept invitation: ia <inviter_username> ";
    cout << "[whatever_message]" << endl;
    cout << "logout: logout" << endl;
}

void LoggedIn(User * user)
{
    char buf[SIZE];
    char message[SIZE];
    char choice[100];
    char name[100];
    char sender_name[100];
    char * temp;
    struct sockaddr_in friend_addr;
    string str_name;
    string str_sender_name;
    int friendfd;

    LoggedInMenu();

    while(1)
    {
        memset(buf, '\0', sizeof(buf));
        memset(message, '\0', sizeof(message));
        memset(choice, '\0', sizeof(choice));
        memset(name, '\0', sizeof(name));
        memset(sender_name, '\0', sizeof(name));

        strcpy(sender_name, user->GetUsername().c_str());

        //get input from user
        fgets(buf, 100, stdin);

        //save choice
        temp = strtok(buf, " \n");

        if(temp != NULL)
        {
            strcpy(choice, temp);
        }

        //save name
        temp  = strtok(NULL, " \n");
        if(temp != NULL)
        {
            strcpy(name, temp);
            str_name = name;
        }

        //save message
        temp = strtok(NULL, "\n");
        if(temp != NULL)
        {
            strcpy(message, temp);
        }

        if(strcmp(choice, "m") == 0)
        {
            memset(&friend_addr, 0, sizeof(friend_addr));

            if((friendfd = user->GetOnlineFriend(str_name, &friend_addr)) == -1)
            {
                cout << "user not online or";
                cout << " not a friend" << endl;
                continue;
            }

            Chat(friendfd, friend_addr, sender_name, str_name, message, user);
        }
        else if(strcmp(choice, "i") == 0)
        {
            //tell server user is inviting
            memset(buf, '\0', sizeof(buf));
            strcat(buf, "invite\n");
            strcat(buf, sender_name);
            strcat(buf, "\n");
            strcat(buf, name);
            strcat(buf, "\n");
            strcat(buf, message);
            strcat(buf, "\n");
            write(user->GetServerFd(), buf, strlen(buf));
        }
        else if(strcmp(choice, "ia") == 0)
        {
            //ensure that invitation exists
            if(user->InvitationExists(name))
            {
                //tell server user is accepting invitation
                memset(buf, '\0', sizeof(buf));
                strcat(buf, "acceptinvite\n");
                strcat(buf, sender_name);
                strcat(buf, "\n");
                strcat(buf, name);
                strcat(buf, "\n");
                strcat(buf, message);
                strcat(buf, "\n");
                write(user->GetServerFd(), buf, strlen(buf));
                //remove invitation after accepting it
                user->RemoveInvitation(name);
            }
            else
            {
                cout << "invite not recieved from user: " << name << endl;
            }
        }
        else if(strcmp(choice, "logout") == 0)
        {
            //clear out data structures, close fds and
            //tell server user is logging out
            user->Logout();
            break;
        }
        else if(strcmp(choice, "of") == 0)
        {
            cout << "online friends" << endl;
            user->PrintOnlineFriends();
        }
        else if(strcmp(choice, "pm") == 0)
        {
            LoggedInMenu();
        }
        else
        {
            cerr << "Invalid Entry" << endl;
        }
    }
}

void Chat(int friendfd, struct sockaddr_in addr, char * friendname, string username, char * message, User * user)
{
    char packet[SIZE];
    int error;
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    user->AddToFdList(sockfd);

    memset(packet, '\0', sizeof(packet));

    if(sockfd == -1)
    {
        cerr << "Can't get socket";
        user->Logout();
        user->CloseServerFd();
        exit(EXIT_FAILURE);
    }

    if(friendfd == 0)
    {
        error = connect(sockfd, (struct sockaddr *)&addr, sizeof(addr));

        user->SetFriendSocketFd(username, sockfd);

        if(error == -1)
        {
            cerr << "Can't connect";
            cerr << "ip = " << inet_ntoa(addr.sin_addr);
            cerr << ", port = " << ntohs(addr.sin_port);
            user->Logout();
            user->CloseServerFd();
            exit(EXIT_FAILURE);
        }
    }

    //send name and message to friend
    strcat(packet, friendname);
    strcat(packet, "\n");
    strcat(packet, message);

    if(friendfd == 0)
    {
        user->AddToFdList(sockfd);
        write(sockfd, packet, strlen(packet));
    }
    else
    {
        user->AddToFdList(friendfd);
        write(friendfd, packet, strlen(packet));
    }
}
void Menu()
{
    cout << "Messenger application." << endl;
    cout << "[r] register" << endl;
    cout << "[l] login" << endl;
    cout << "[exit] exit" << endl;
}
