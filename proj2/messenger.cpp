#include "messenger.h"

void User::Initialize(string config_file)
{
    ifstream fin;
    vector<char *> login_choices;
    string line;
    int itr, itr2;
    string temp2;

    already_listening = 0;

    memset(buf, '\0', sizeof(buf));

    fin.open(config_file);

    if(!fin.is_open())
    {
        cerr << "Unable to open file\n";
        exit(EXIT_FAILURE);
    }

    getline(fin, line);
    itr = line.find_first_of(":");

    temp2 = line.substr(0, itr);
    itr2 = line.find_first_of("\n");

    if(temp2.compare("servhost") == 0)
    {
        server_host = line.substr(itr+1, itr2);

        //get second line
        getline(fin, line);
        itr = line.find_first_of(":");
        temp2 = line.substr(0, itr);
        if(temp2.compare("servport") == 0)
        {
            itr2 = line.find_first_of("\n");
            server_port = line.substr(itr+1, itr2);
        }
        else
        {
            cerr << "ERROR: bad file format" << endl;
        }
    }
    else if(temp2.compare("servport") == 0)
    {
        server_port = line.substr(itr+1, itr2);

        //get second line
        getline(fin, line);
        itr = line.find_first_of(":");
        temp2 = line.substr(0, itr);
        if(temp2.compare("servhost") == 0)
        {
            itr2 = line.find_first_of("\n");
            server_host = line.substr(itr+1, itr2);
        }
        else
        {
            cerr << "ERROR: bad file format" << endl;
        }
    }
    else
    {
        cerr << "ERROR: invalid keyword\n" << endl;
        exit(EXIT_FAILURE);
    }

    fin.close();

    ConnectToServer();
}

void User::ConnectToServer()
{
    struct hostent * he;
    char hostname[1024];
    char ip[1024];
    struct in_addr **addr_list;

    hostname[1023] = '\0';
    gethostname(hostname, 1023);
    he = gethostbyname(hostname);
    addr_list = (struct in_addr **)he->h_addr_list;

    memset(&my_location, 0, sizeof(struct sockaddr_in));
    memset(ip, '\0', sizeof(ip));

    strcpy(ip, inet_ntoa(*addr_list[0]));
    my_location.sin_family = AF_INET;
    my_location.sin_port = htons(CLIENT_LISTEN_PORT);
    inet_aton(ip, &my_location.sin_addr);


    int error;

    //create a tcp socket and return the socket fd
    server_fd = socket(AF_INET, SOCK_STREAM, 0);

    //check that socket() worked
    if(GetServerFd() == -1)
    {
        cerr << "Can't get socket";
        exit(EXIT_FAILURE);
    }

    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons((short)atoi(server_port.c_str()));
    addr.sin_addr.s_addr = inet_addr(server_host.c_str());

    //connect the socket file descriptor to address specified by addr
    error = connect(GetServerFd(), (struct sockaddr *)&addr, sizeof(addr));


    if(error == -1)
    {
        cerr << "Can't connect";
        exit(EXIT_FAILURE);
    }
    else
    {
        cout << "CONNECTED TO SERVER" << endl;
        cout << "-------------------------------------" << endl;
        cout << "ip = " << inet_ntoa(addr.sin_addr) << endl;
        cout << "port = " << ntohs(addr.sin_port) << endl;
        cout << "-------------------------------------" << endl;
        cout << "USER LOCATION" << endl;
        cout << "-------------------------------------" << endl;
        cout << "ip = " << inet_ntoa(my_location.sin_addr) << endl;
        cout << "port = " << ntohs(my_location.sin_port) << endl;
        cout << "-------------------------------------" << endl;
    }
}

int User::Register()
{
    char buf[SIZE];
    char c_uname[100];
    char c_pass[100];
    string response;

    //clear out buffers
    memset(buf, '\0', sizeof(buf));
    memset(c_uname, '\0', sizeof(c_uname));
    memset(c_pass, '\0', sizeof(c_pass));

    strcat(buf, "r\n");

    //get username
    cout << "Enter username: " << flush;
    fgets(c_uname, sizeof(c_uname), stdin);
    strcat(buf, c_uname);

    //get password
    cout << "Enter password: " << flush;
    fgets(c_pass, sizeof(c_pass), stdin);
    strcat(buf, c_pass);

    //sending register packet
    write(GetServerFd(), buf, strlen(buf));

    //get response from server
    read(GetServerFd(), buf, 4);
    response = buf;

    //username taken -- error
    if(response == "500")
    {
        return 0;
    }

    //username available - set as username
    else if(response == "200")
    {
        username = c_uname;
        password = c_pass;
        return 1;
    }

    return -1;
}

int User::Login()
{
    char c_usr[100];
    char c_pass[100];
    char resp[4];
    char c_ip[100];
    short port;
    string response;

    //get server username
    printf("Enter username: ");
    fflush(0);

    memset(c_usr, '\0', sizeof(c_usr));
    fgets(c_usr, SIZE, stdin);
    strcat(buf, c_usr);

    //get server password
    printf("Enter password: ");
    fflush(0);

    memset(c_pass, '\0', sizeof(c_pass));
    fgets(c_pass, 100, stdin);
    strcat(buf, c_pass);

    //send login data to server
    write(GetServerFd(), buf, sizeof(buf));

    //get response
    read(GetServerFd(), resp, 4);
    response = resp;

    //invalid username/password -- error
    if(response == "500")
    {
        return 0;
    }
    else if(response == "200")
    {
        //user has logged in successfully

        //let pthreads run
        stop_pthread = 0;

        //sending location info
        c_usr[strcspn(c_usr, "\n")] = '\0';
        username = c_usr;

        //sending ip
        strcpy(c_ip, inet_ntoa(my_location.sin_addr));
        write(GetServerFd(), c_ip, strlen(c_ip));

        //sending port
        port = my_location.sin_port;
        write(GetServerFd(), &port, sizeof(port));

        return 1;
    }

    //if response did not return 200 or 500
    //a server error occured
    return 0;
}

int User::InvitationExists(string name)
{
    for(auto itr = invitations.begin(); itr != invitations.end(); itr++)
    {
        //invitation exists
        if(itr->compare(name) == 0)
        {
            return 1;
        }
    }

    //if the entire invitations vector is checked,
    //the invitation doesn't exist
    return 0;
}

int User::RemoveInvitation(string name)
{
    //remove invitation from vector of invitations
    for(auto itr = invitations.begin(); itr != invitations.end(); itr++)
    {
        //removing invitation
        if(itr->compare(name) == 0)
        {
            invitations.erase(itr);
            return 1;
        }
    }

    return 0;
}

//close all open file descriptors
//clear out list of online users
void User::Logout()
{
    char buf[SIZE];
    memset(buf, '\0', sizeof(buf));
    strcat(buf, "logout\n");
    strcat(buf, GetUsername().c_str());
    strcat(buf, "\n");

    //tell server user is logging out
    write(GetServerFd(), buf, strlen(buf));

    //stop all pthreads
    stop_pthread = 1;

    //close all file descriptors
    for(auto itr = fd_list.begin(); itr != fd_list.end(); itr++)
    {
        close(*itr);
    }
    //clear out list
    fd_list.clear();
    //clear out list of online friends
    online_friends.clear();
    //clear out list of invitations
    invitations.clear();
}

void User::AcceptInvitation(string name, string msg)
{
    cout << "invite accept " << name << " >> " << msg << endl;
}

int User::AddInvitation(string name, string msg)
{
    cout << "invitation " << name << " >> " << msg << endl;

    //add invitation to vector of invitations
    for(auto itr = invitations.begin(); itr != invitations.end(); itr++)
    {
        //invitation has already been sent
        if(itr->compare(name) == 0)
        {
            return 0;
        }
    }
    //new invitation -- add to vector
    invitations.push_back(name);
    return 1;
}

void User::PrintOnlineFriends()
{
    for(auto& x: online_friends)
    {
        cout << "name: " << x.first << "\t";
        cout << "ip: " << inet_ntoa(x.second.addr.sin_addr) << "\t";
        cout << "port: " << ntohs(x.second.addr.sin_port);
        cout << "\tfd: " << x.second.sockfd;
        cout << endl;
    }
}

int User::AddOnlineFriend(string str, struct sockaddr_in location)
{
    struct location_info temp;
    temp.addr = location;
    temp.sockfd = -1;

    auto itr = online_friends.find(str);

    //friend not added to list yet
    if(itr == online_friends.end())
    {
        online_friends.insert({str, temp});
        cout << "--------------------------------" << endl;
        cout << "ONLINE" << endl;
        cout << "user: " << str << endl;
        cout << "ip: " << inet_ntoa(location.sin_addr) << endl;
        cout << "port: " << ntohs(location.sin_port) << endl;
        cout << "--------------------------------" << endl;
        return 1;
    }

    //friend already added otherwise
    return 0;
}


int User::RemoveOnlineFriend(string str)
{
    auto itr = online_friends.find(str);

    //friend not removed yet removed
    if(itr != online_friends.end())
    {
        online_friends.erase(str);
        cout << "--------------------------------" << endl;
        cout << "OFFLINE" << endl;
        cout << "user: " << str << endl;
        cout << "--------------------------------" << endl;
        return 1;
    }

    //friend already removed otherwise
    return 0;
}

int User::GetOnlineFriend(string str, struct sockaddr_in * arg_addr)
{
    auto itr = online_friends.find(str);

    //friend could not be found
    if(itr == online_friends.end())
    {
        return -1;
    }
    //connection between friends has already been established
    //return the socketfd between them
    if(itr->second.sockfd > 0)
    {
        return itr->second.sockfd;
    }

    (*arg_addr) = itr->second.addr;
    return 0;
}

string User::GetUsername()
{
    return username;
}

void User::SetFriendSocketFd(string str, int fd)
{
    auto itr = online_friends.find(str);

    itr->second.sockfd = fd;
}

void User::AddToFdList(int fd)
{
    for(auto itr = fd_list.begin(); itr != fd_list.end(); itr++)
    {
        //fd already in list or an invalid number
        //stdin = 0, stdout = 1, stderr = 2
        if(((*itr) == fd) || (fd < 3))
        {
            return;
        }
    }
    //fd not in list
    fd_list.push_back(fd);
}

int User::GetServerFd()
{
    return server_fd;
}

void User::CloseServerFd()
{
    //send out a message to the server
    //telling server that connection is over
    char buf[100];
    memset(buf, '\0', sizeof(buf));
    strcat(buf, "CLIENT_EXIT\n");
    write(server_fd, buf, strlen(buf));
    //close connection to server
    close(server_fd);
}

int User::TerminateThread()
{
    return stop_pthread;
}


unordered_map<string, struct location_info> User::GetOnlineFriends()
{
    return online_friends;
}

//constructor, saves info from user_info_file to hash table
//Server::Server(string filename)
void Server::FillTable(string filename)
{
    size_t itr = 0;
    size_t itr2 = 0;
    size_t len = 0;
    size_t newline = 0;
    string username;
    string password;
    vector<string> contacts;
    string str;
    ifstream fin;
    string line;
    struct UserInfo temp;

    //number of online users starts at 0
    num_online = 0;

    memset(&temp.addr, 0, sizeof(struct sockaddr_in));
    temp.online = 0;

    //open user file
    fin.open("user_info_file", ios::in);

    if(!fin.is_open())
    {
        cerr << "Unable to open file\n";
        exit(EXIT_FAILURE);
    }

    //save user file info into the hash table
    while(getline(fin, line))
    {
        itr = 0;
        itr2 = 0;
        len = 0;
        itr = line.find_first_of("|", itr+1);
        itr2 = line.find_first_of("|", itr+1);
        len = itr2-itr;
        //saving username to hash table
        temp.username = line.substr(0,itr);
        //saving password to hash table
        temp.password = line.substr(itr+1, len-1);
        itr = itr2;
        itr2 = line.find_first_of(";", itr+1);
        len = itr2-itr;
        newline = line.find_first_of("\n");
        //saving the first contact to hash table
        if(!(line.substr(itr+1, len-1).empty()))
        {
            temp.contact_list.push_back(line.substr(itr+1, len-1));

            itr = itr2;
            //saving all other contacts to hash table
            while(itr < newline)
            {
                if((itr2 = line.find_first_of(";", itr+1)))
                {
                    len = itr2-itr;
                    temp.contact_list.push_back(line.substr(itr+1, len-1));
                }
                itr = itr2;
                itr2 = line.find_first_of(";", itr+1);
                len = itr2-itr;
            }
        }
        hash_table.insert({temp.username, temp});
        temp.contact_list.clear();
    }
    fin.close();
}

// print out the hash table of users
void Server::PrintTable()
{
    for(auto& x: hash_table)
    {
        cout << "key: " << x.first << endl;
        cout << "data:\n\tusername: '" << x.second.username << "'";
        cout << "\n\tpassword: '" << x.second.password << "'";
        cout << "\n\tcontact list: ";
        for(auto& y: x.second.contact_list)
        {
            cout << y << ", ";
        }
        cout << "ip = " << inet_ntoa(x.second.addr.sin_addr);
        cout << ", port = " << ntohs(x.second.addr.sin_port) << endl;
        cout << "\tonline: ";
        if(x.second.online == 1)
        {
            cout << "yes";
        }
        else
        {
            cout << "no";
        }
        cout << endl << endl;
    }
}

//check to see if username is already in hash table
//0 if username is unavailable, 1 if available
int Server::UsernameAvailable(string name)
{
    auto itr = hash_table.find(name);

    //username not found -- username available
    if(itr == hash_table.end())
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

//check to see if passwords match between the supplied
//and the known user password. returns -1 if username
//not found, 0 if incorrect password, 1 if working
int Server::CheckLogin(string name, string pass)
{
    int value;

    unordered_map<std::string, struct UserInfo>::iterator itr;

    itr = hash_table.find(name);

    //username found
    if(itr != hash_table.end())
    {
        value = pass.compare((itr->second.password));
        if(value == 0)
        {
            //passwords match
            return 1;
        }
        else
        {
            //cout << "incorrect password" << endl;
            return 0;
        }
    }
    else
    {
        //cout << "username not found" << endl;
        return 0;
    }
}

//add user to hash_table
int Server::AddUser(string name, string pass)
{
    vector<string> contacts;
    struct UserInfo temp;

    temp.username = name;
    temp.password = pass;
    temp.contact_list = contacts;

    hash_table.insert({temp.username, temp});

    return 0;
}

struct sockaddr_in Server::GetLocationInfo(string username)
{
    auto itr = hash_table.find(username);
    return itr->second.addr;
}

void Server::AddLocationInfo(string username, struct sockaddr_in addr)
{
    //save ip address to sockaddr_in struct
    //it = hash_table.find(username);
    auto itr = hash_table.find(username);

    if(itr != hash_table.end())
    {
        itr->second.addr = addr;
    }

    //it->second.addr.sin_addr = addr->sin_addr;

    //save port number to sockaddr_in struct
    //it->second.addr.sin_port = addr->sin_port;
}

int Server::GetUserFd(string username)
{
    auto itr = hash_table.find(username);
    if(itr != hash_table.end())
    {
        return itr->second.fd;
    }
    return 0;
}

void Server::AddUserFd(string username, int fd)
{
    auto itr = hash_table.find(username);
    if(itr != hash_table.end())
    {
        itr->second.fd = fd;
    }
}

void Server::SendOfflineBroadcast(string username, int sockfd)
{
    char buf[SIZE];
    char c_usr[100];
    memset(buf, '\0', sizeof(buf));
    memset(c_usr, '\0', sizeof(c_usr));
    strcpy(c_usr, username.c_str());

    strcat(buf, "OFFLINE\n");
    strcat(buf, c_usr);
    strcat(buf, "\n");

    //cout << buf << endl;
    write(sockfd, buf, strlen(buf));
}

void Server::AcceptInvitation(string name, string invited_name, string msg)
{
    char buf[SIZE];
    int sockfd;

    memset(buf, '\0', sizeof(buf));

    strcat(buf, "ACCEPT_INVITE\n");
    strcat(buf, name.c_str());
    strcat(buf, "\n");
    strcat(buf, msg.c_str());
    strcat(buf, "\n");

    //get the fd of the person who sent the invitation
    sockfd = GetUserFd(invited_name);

    //update contact list of person who sent the invitation
    auto itr = hash_table.find(invited_name);
    if(itr != hash_table.end())
    {
        itr->second.contact_list.push_back(name);
    }

    //update contact list of person who got the invitation
    itr = hash_table.find(name);
    if(itr != hash_table.end())
    {
        itr->second.contact_list.push_back(invited_name);
    }

    //send accept invite to user who sent the invitation
    write(sockfd, buf, strlen(buf));
}

void Server::SendInvitation(string name, string invited_name, string msg)
{
    char buf[SIZE];
    int sockfd;

    memset(buf, '\0', sizeof(buf));

    strcat(buf, "INVITE\n");
    strcat(buf, name.c_str());
    strcat(buf, "\n");
    strcat(buf, msg.c_str());
    strcat(buf, "\n");

    //get the fd of the person being invited
    sockfd = GetUserFd(invited_name);

    //send invitation to the invited user
    write(sockfd, buf, strlen(buf));
}

int Server::SendLocationInfo(string username, int sockfd)
{
    char buf[SIZE];
    char c_ip[100];
    char c_port[100];
    //get correct user
    auto itr = hash_table.find(username);

    //clear out buffers
    memset(buf, '\0', sizeof(buf));
    memset(c_ip, '\0', sizeof(c_ip));
    memset(c_port, '\0', sizeof(c_port));

    //loop through the user's contact list
    //          ________ __ __________ __ ___________
    //writing: |username|\n|ip_address|\n|port_number|
    //
    //for each of the user's contacts

    if(itr != hash_table.end())
    {
        auto contact_entry = itr->second.contact_list;
        strcat(buf, "ONLINE\n");

        for(unsigned int i = 0; i < contact_entry.size(); i++)
        {
            auto f_entry = hash_table.find(contact_entry[i]);

            if(f_entry != hash_table.end() && f_entry->second.online == 1)
            {
                //add username to buffer
                strcat(buf, f_entry->second.username.c_str());
                //add newline delimiter
                strcat(buf, "\n");
                //add ip address to buffer
                strcpy(c_ip, inet_ntoa(f_entry->second.addr.sin_addr));
                strcat(buf, c_ip);
                memset(c_ip, '\0', sizeof(c_ip));
                //add newline delimiter to buffer
                strcat(buf, "\n");
                //add port number to buffer
                sprintf(c_port, "%d", (f_entry->second.addr.sin_port));
                strcat(buf, c_port);
                memset(c_port, '\0', sizeof(c_port));
                //add newline delimiter to buffer
                strcat(buf, "\n");
            }
        }
    }

    //write friend info to file
    write(sockfd, buf, strlen(buf));

    return 0;
}

//tells user server has quit unexpectedly
void Server::ServerError(int fd)
{
    char buf[100];
    memset(buf, '\0', sizeof(buf));
    strcat(buf, "SERVER_ERROR\n");
    write(fd, buf, strlen(buf));
}

//sets user to online
void Server::SetOnline(string username)
{
    auto itr = hash_table.find(username);
    if(itr != hash_table.end())
    {
        itr->second.online = 1;
        num_online++;
    }
}

void Server::SetOffline(string username)
{
    auto itr = hash_table.find(username);
    if(itr != hash_table.end())
    {
        //set online to 0
        itr->second.online = 0;
        //empty out location info
        memset(&(itr->second.addr), 0, sizeof(itr->second.addr));
        //clear out fd
        itr->second.fd = 0;

        if(num_online > 0)
        {
            num_online--;
        }
    }
}

vector<struct UserInfo> Server::GetOnlineFriends(string username)
{
    vector<struct UserInfo> temp;
    string f_name;
    auto itr = hash_table.find(username);

    for(unsigned i = 0; i < itr->second.contact_list.size(); i++)
    {
        //get name of friend from contact list
        f_name = itr->second.contact_list[i];
        //find friend in hash table
        auto j = hash_table.find(f_name);
        //see if friend is online
        if(j != hash_table.end() && j->second.online == 1)
        {
            //add friend to vector if online
            temp.push_back(j->second);
        }
    }

    return temp;
}

vector<string> Server::GetOnlineUsers()
{
    vector<string> temp;

    for(auto& x: hash_table)
    {
        //push back online users
        if(x.second.online == 1)
        {
            temp.push_back(x.first);
        }
    }
    return temp;
}

unordered_map<string, struct UserInfo> Server::GetTable()
{
    return hash_table;
}

int Server::NumOnlineUsers()
{
    return num_online;
}
