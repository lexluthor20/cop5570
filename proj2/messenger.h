#ifndef MESSAGE_H
#define MESSAGE_H

#define SIZE 1024
#define CLIENT_LISTEN_PORT 5100

#include <algorithm>
#include <arpa/inet.h>
#include <cstddef>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <stack>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <unordered_map>
#include <vector>

using namespace std;

struct location_info
{
    struct sockaddr_in addr;
    int sockfd;
};

struct UserInfo
{
    string username;
    string password;
    vector<string> contact_list;
    struct sockaddr_in addr;
    char buf[100];
    int online;
    int fd;
};


class User
{
    public:
        void Initialize(string config_file);
        int Register(); //register user
        int Login();//login user
        int Exit(); //let user exit
        int Chat(); //chat with a user
        int Invitation(int fd);//invite a friend to chat
        int AcceptInvitation(); //accept invitation
        void Logout();//user can logout
        int InvitationExists(string name);
        int RemoveInvitation(string name);
        void AcceptInvitation(string name, string msg);
        int AddInvitation(string name, string msg);
        void PrintOnlineFriends();
        unordered_map<string, struct location_info> GetOnlineFriends();
        int AddOnlineFriend(string str, struct sockaddr_in location);
        int RemoveOnlineFriend(string str);
        int GetOnlineFriend(string str, struct sockaddr_in * arg_addr);
        int GetServerFd();
        void CloseServerFd();
        string GetUsername();
        void SetFriendSocketFd(string str, int fd);
        void AddToFdList(int fd);
        int TerminateThread();
        char buf[SIZE];
        User operator=(string config_file);
        int already_listening;
    private:
        void ConnectToServer();
        int AddContact(string str);
        int RemoveContact(string str);
        string username;
        string password;
        struct sockaddr_in addr;
        struct sockaddr_in my_location;
        struct sockaddr_in chat_addr;
        unordered_map<string, struct location_info> online_friends;
        stack<pthread_t> pthread_stack;
        vector<string> invitations;
        vector<int> fd_list;
        string server_host;
        string server_port;
        int server_fd;
        int stop_pthread;
};

class Server
{
    public:
        void FillTable(string filename); //fills up hashtable
        void PrintTable();//print hash table
        int UsernameAvailable(string name); //check if username is available
        int CheckLogin(string name, string pass); //compare passwords
        int AddUser(string name, string pass);
        void AddLocationInfo(string username, struct sockaddr_in addr);
        struct sockaddr_in GetLocationInfo(string username);
        void AddUserFd(string username, int fd);
        int GetUserFd(string username);
        int SendLocationInfo(string username, int fd);
        void SetOnline(string username); //sets user to online
        void SetOffline(string username); //sets user to offline
        void SendOfflineBroadcast(string username, int sockfd);
        void AcceptInvitation(string name, string invited_name, string msg);
        void SendInvitation(string name, string invited_name, string msg);
        void ServerError(int fd);
        vector<struct UserInfo> GetOnlineFriends(string username); //list of online friends
        vector<string> GetOnlineUsers();
        unordered_map<string, struct UserInfo> GetTable();
        int NumOnlineUsers();
    private:
        std::unordered_map<std::string, struct UserInfo> hash_table;
        int num_online;
};

#endif
