import requests
import multiprocessing
import sys
import getpass
import stem.connection
from stem.control import Controller
import pycurl
from io import BytesIO
import certifi
import time
import statistics
import matplotlib.pyplot as plt
import threading
import signal
import os


master_list = []

SOCKS_PORT = 9050
CONNECTION_TIMEOUT = 30 #timeout before we give up on a circuit

####################################################################
#queries the onionoo web service for the
#flag parameter and returns a list of fingerprints
#matching that flag
def GetRelays(flag, result):
    url = 'https://onionoo.torproject.org/'

    #options: summary, details, bandwith, weights, clinets, uptime
    infotype = 'summary'

    temp_list = []
    payload = { 'type' : 'relay',   #return only relays
                'running' : 'true', #only running relays
                'flag' : flag,      #flag
                'limit' : '30'}     #limit 200 responses

    r = requests.get(url+infotype, params=payload)
    relays = r.json()["relays"]

    for x in relays:
        result.append(x['f'])
####################################################################

####################################################################
def GetMasterList(multi_list):
    master_list = multi_list[0]
    #iterates through each list in the multi-list
    for sub_list in multi_list:
        #goes through each item in the master list
        for item in master_list:
            #checks if the item in the list is in
            #the master list as well
            if item not in sub_list:
                master_list.remove(item)

    return master_list
####################################################################

####################################################################
def TorQuery(url):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.PROXY, 'localhost') #setup proxy to localhost for tor
    c.setopt(c.PROXYPORT, SOCKS_PORT) #setup proxy to tor port
    c.setopt(c.PROXYTYPE, pycurl.PROXYTYPE_SOCKS5_HOSTNAME)
    c.setopt(c.CONNECTTIMEOUT, CONNECTION_TIMEOUT)
    c.setopt(c.WRITEDATA, buffer)
    c.setopt(c.CAINFO, certifi.where())
    c.perform()
    c.close()
    data = buffer.getvalue()
    return str(data)
###################################################################

###################################################################
def PlotStats(x, stats):
    plt.bar(x,stats)
    plt.title("tor circuit runtimes")
    plt.xlabel("relays")
    plt.ylabel("runtimes")
    plt.show()
###################################################################

###################################################################
def GetStats():
    stats_list = []
    x_axis = []
    counter = 0
    for m in master_list:
        if isinstance(m[1], float):
            stats_list.append(m[1])
            counter += 1
            x_axis.append(counter)

    plot = multiprocessing.Process(target=PlotStats, \
            args=(x_axis, stats_list))
    plot.start()

    average = statistics.mean(stats_list)
    standard_deviation = statistics.stdev(stats_list)
    variance = statistics.variance(stats_list)
    minimum = min(stats_list)
    maximum = max(stats_list)

    stats_dict = {'average' : average, \
                  'standard_deviation': standard_deviation, \
                  'variance' : variance, \
                  'minimum' : minimum, \
                  'maximum' : maximum}

    print('average run-time for the circuits: ', \
            stats_dict['average'])

    print('standard deviation for the circuits: ', \
            stats_dict['standard_deviation'])

    print("variance for the circuits: ", \
            stats_dict['variance'])

    print("minimum runtime for the circuits: ", \
            stats_dict['minimum'])

    plot.join()

    return stats_dict
####################################################################
def UseData(stats_dict):
    min_guard = []
    min_exit = []
    min_hop = []
    max_guard = []
    max_exit = []
    max_hop = []
    minimum = stats_dict['minimum']
    maximum = stats_dict['maximum']
    std_dev = stats_dict['standard_deviation']
    #get a list of guard, exit, and hop fingerprints
    #that are within one standard deviation of the minimum
    for m in master_list:
        if (isinstance(m[1], float)) and \
                (m[1] <= (minimum + std_dev)):
            #guard node
            if m[0][0] not in min_guard:
                min_guard.append(m[0][0])
            #hop node
            if m[0][1] not in min_hop:
                min_hop.append(m[0][1])
            #exit node
            if m[0][2] not in min_exit:
                min_exit.append(m[0][2])

    for m in master_list:
        if isinstance(m[1], float) and \
                (m[1] >= (maximum - std_dev)):
            #guard node
            if (m[0][0] not in max_guard) and \
                    (m[0][0] not in min_guard):
                max_guard.append(m[0][0])
            if (m[0][1] not in max_hop) and \
                    (m[0][1] not in min_hop):
                max_hop.append(m[0][1])
            if (m[0][2] not in max_exit) and \
                    (m[0][2] not in min_exit):
                max_exit.append(m[0][2])

    usable_dict = {'min_guard' : min_guard, \
                    'min_hop' : min_hop, \
                    'min_exit' : min_exit, \
                    'max_guard' : max_guard, \
                    'max_hop' : max_hop, \
                    'max_exit' : max_exit}

    print(usable_dict)
    return usable_dict
####################################################################
def UpdateTorrc(data_dict):
    while True:
        torrc_path = input("Please specify the FULL path of your torrc file: ")
        if os.path.exists(torrc_path):
            if os.path.basename(torrc_path) == 'torrc':
                #open torrc for appending
                f = open(torrc_path, 'a')
                #write entry nodes
                f.write('EntryNodes ')
                for entrynode in data_dict['min_guard']:
                    f.write(entrynode)
                    f.write(', ')
                f.write('\n')
                #write layer2 nodes
                f.write('HSLayer2Nodes ')
                for layer2node in data_dict['min_hop']:
                    f.write(layer2node)
                    f.write(', ')
                f.write('\n')
                f.write('ExitNodes ')
                #write exit nodes
                for exitnode in data_dict['min_exit']:
                    f.write(exitnode)
                    f.write(', ')
                f.write('\n')
                f.write('ExcludeNodes ')
                #write exclude nodes
                for excludenode in data_dict['max_guard']:
                    f.write(excludenode)
                    f.write(', ')
                for excludenode in data_dict['max_hop']:
                    f.write(excludenode)
                    f.write(', ')
                for excludenode in data_dict['max_exit']:
                    f.write(excludenode)
                    f.write(', ')
                f.write('\n')
                f.close()
                break
            else:
                print('bad file name: ', end='')
                print(os.path.basename(torrc_path))
                print('file needs to be named torrc')
        else:
            print('path specified does not exist')
####################################################################

####################################################################
def SigintHandler(sig, frame):
    print("Ending data retrival early")
    stats = GetStats()
    choice = input("keep testing circuits? [y/n]: ")
    while True:
        if choice.upper() == 'N':
            UseData(stats)
            UpdateTorrc(UseData(stats))
            sys.exit(1)
        elif choice.upper() == 'Y':
            return
        else:
            print("invalid choice")
            choice = input("keep testing circuits? [y/n]: ")

####################################################################

signal.signal(signal.SIGINT, SigintHandler)

#creating a controller to mess with the circuit that tor takes
try:
    controller = Controller.from_port()
except stem.SocketError as exc:
    print('unable to connect to port 9051 (%s)' %  exc)
    sys.exit(1)

try:
    controller.authenticate()
except stem.connection.MissingPassword:
    pw = getpass.getpass("Controller pasword: ")
    try:
        controller.authenticate(password=pw)
    except stem.connection.PasswordAuthFailed:
        print("unable to authenticate, password is incorrect")
        sys.exit(1)
    except stem.connection.AuthenticationFailure as exc:
        print("unable to authenticate: %s" % exc)
        sys.exit(1)

print("tor is running version %s" % controller.get_version())


#gets a single list of all the flags requested
#runs each onionoo query set on a seperate thread to reduce runtime
stable = []
t1 = threading.Thread(target=GetRelays, args=('stable', stable))
guard = []
t2 = threading.Thread(target=GetRelays, args=('guard', guard))
running = []
t3 = threading.Thread(target=GetRelays, args=('running', running))
exit = []
t4 = threading.Thread(target=GetRelays, args=('exit', exit))

t1.start()
t2.start()
t3.start()
t1.join()
t2.join()
t3.join()

#filter down all flags we want for our guards
guard = GetMasterList([guard, stable, running])
#filter down all flags we want for our exit
exit = GetMasterList([stable, running])
#filter down all flags we want for our hop
hop = GetMasterList([stable, running])

#generate lists of length three for circuits
#lists are formatted as [guard hop exit]
#to get a circuit of the form: guard_n => hop_n => exit_n
for x in guard:
    for y in hop:
        for z in exit:
            templist = []
            if x != y and x != z and y != z:
                templist = [[x,y,z],[]]
                #if len(master_list) < 8:
                master_list.append(templist)

for m in master_list:
    try:
        #block until circuit is generated
        c = controller.new_circuit(m[0], await_build=True)
        #time stamp
        start = time.time()
        tor_check = TorQuery('https://check.torproject.org')
        if "Congratulations. This browser is configured to use Tor."\
        not in tor_check:
            print("unable to tunnel data through tor")
        end = time.time()
        controller.close_circuit(c)
        time_taken = end-start
        #print(m, ' => ', time_taken)
        m[1] = time_taken
        print(m[1])
    except Exception as e:
        print(m, ' => ', e)

UpdateTorrc(UseData(GetStats()))
