matplotlib==3.0.2
certifi==2018.1.18
pycurl==7.43.0.2
requests==2.18.4
stem==1.7.0
