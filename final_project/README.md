Onion Slicer
====================================================================

Onion Slicer is a python3 tor network optimizer. It removes the
variance between different connection times when using Tor during
different sessions. By removing anomalies when using Tor, it allows
people using the Tor browser to be able to expect a consistent user
experience.

To Install
====================================================================
Note: These packages were installed on Ubuntu. If you are using a
different package managager, replace **apt** with your package
manager

1. Install the Tor Browser from 
    - **https://www.torproject.org/download/download-easy.html.en**
    - Let this install in the background while you complete the
    other steps
2. Install Python3 if it is not already installed
    - `sudo apt install python3`
3. Install pip3 if it is not already installed
    - `sudo apt install python3-pip`
4. Update python setuptools
    - `pip3 install --upgrade setuptools`
5. Install libcurl 
    - `sudo apt install libcurl14-openssl-dev libssl-dev`
6. Install the required python3 modules
    - `pip3 install -r requirements.txt`
7. Install python3 tkinter
    - `sudo apt install python3-tk`
8. Install Tor command line tool
    - `sudo apt install tor`
9. Open the torrc file of the command line tool (in /etc/tor/torrc)
    - On line 18, where it says,
    > SOCKSPort 9050
    - delete the first `#` on the line
    - On line 57, where it says,
    > ControlPort 9051
    delete the first `#` on the line
    - On line 61, where it says,
    > CookieAuthentication 1
    - delete the first `#` on the line
10. Restart Tor command line tool
    - `sudo /etc/init.d/tor`

To Run
====================================================================
1. Run `sudo python3 main.py`
2. Let the program run for as long as you want
    - Note: The longer you let Onion Slicer run, the better
    performance you will end up getting
    - Hit Ctrl-C to end the circuit search
3. The program will now display a bar graph showing the runtimes of
    each relay
    - An example is given:
    ![picture alt](results.png)
4. When you are done viewing this, click the `x` in the window
5. Type in the **FULL** pathname of the browser's torrc file
    - to do this try:
       1. `cd ~`
       2. `find . -name tor-browser_en-US`
       3. `cd X` where X = the printout of the above command
       4. `find . -name torrc`
       5. `cd X` where X = the printout of the above command
       without the torrc portion of the printout
       6. `pwd`
       7. Take this printout, copy it into the terminal running
       Onion Slicer, and add `/torrc` and click **Enter**
6. Now open your Tor Browser. You should be able to run Tor now 
    without any extremely slow circuits
    - An example of the updated torrc file is given:
    ![picture alt](torrc_example.png)
